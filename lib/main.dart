import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hs_kingbuy/utils/routes.dart';
import 'package:provider/provider.dart';
import 'presentation/presentation.dart';

void main() {

  //block landscape mode
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MainHomeViewModel()..init(),
        )
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'King Buy',
      home: HomeScreen(),
      routes: Routes.routes,
    );
  }
}
