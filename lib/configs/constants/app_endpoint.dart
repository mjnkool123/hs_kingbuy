class AppEndpoint {
  AppEndpoint._();

  static const String BASE_URL = "https://kingbuy.sapp.asia/";

  static const int connectionTimeout = 1500;
  static const int receiveTimeout = 1500;
  static const String keyAuthorization = "Authorization";

  static const int SUCCESS = 200;
  static const int ERROR_TOKEN = 401;
  static const int ERROR_VALIDATE = 422;
  static const int ERROR_SERVER = 500;
  static const int ERROR_DISCONNECT = -1;

  static const String ALLPROMONTION = 'api/getAllPromotion';
  static const String ALLMYPROMOTION = 'api/getAllMyPromotion';
  static const String CATEGORIES = 'api/getAllCategories';
  static const String ALLPRODUCTNEW = 'api/getAllProductNew';
  static const String PRODUCTSBYCATEGORY = 'api/getProductsByCategory';
  static const String ALLADDRESS = 'api/getAllAddress';
  static const String REVIEWPRODUCT = 'api/getReviewByProduct';
  static const String RATINGINFO = 'api/ratingInfoByProduct';
  static const String PRODUCTQUESTION = 'api/getProductQuestions';
  static const String PURCHASEDTOGETHERPRODUCT = 'api/purchasedTogetherProducts';
  static const String RELATEDPRODUCT = 'api/relatedProduct';
  static const String SEARCH = 'api/searchProduct';
}
