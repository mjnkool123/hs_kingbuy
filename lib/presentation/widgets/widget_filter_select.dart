import 'package:flutter/material.dart';

class FilterSelect extends StatefulWidget {
  final List<Widget> children;
  final Function(dynamic) onChanged;
  final TextStyle textStyle;
  final TextStyle selectStyle;
  final BoxDecoration decoration;
  final BoxDecoration selectDecoration;
  final EdgeInsets paddingContent;
  final double space;
  final dynamic defaultSelect;
  final MainAxisAlignment mainAxisAlignment;

  FilterSelect(
      {Key key,
        @required this.children,
        @required this.onChanged,
        this.textStyle,
        this.decoration,
        this.paddingContent,
        this.selectStyle,
        this.defaultSelect,
        this.space = 0,
        this.mainAxisAlignment,
        this.selectDecoration})
      : super(key: key);

  @override
  _FilterSelectState createState() => _FilterSelectState();
}

class _FilterSelectState extends State<FilterSelect> {
  var _selectValue;

  @override
  void initState() {
    if (widget.defaultSelect != null) {
      _selectValue = widget.defaultSelect;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: widget.mainAxisAlignment ?? MainAxisAlignment.start,
      children: _button(),
    );
  }
  _button() {
    final widgets = <Widget>[];
    for (var i = 0; i < widget.children.length; i++) {
      final value = widget.children[i];
      widgets.add(GestureDetector(
        onTap: () {
          widget.onChanged(value);
          setState(() {
            _selectValue = value;
          });
        },
        child: Container(
          padding: widget.paddingContent ?? EdgeInsets.zero,
          margin: EdgeInsets.only(
            left: 0,
            right: i == widget.children.length - 1 ? 0 : widget.space,
          ),
          decoration: value == _selectValue
              ? widget.selectDecoration ?? BoxDecoration()
              : widget.decoration ?? BoxDecoration(),
          child: DefaultTextStyle(
            style: value == _selectValue
                ? widget.selectStyle ?? TextStyle(color: Colors.black)
                : widget.textStyle ?? TextStyle(color: Colors.black),
            child: widget.children[i],
          ),
        ),
      ));
    }
    return widgets;
  }
}