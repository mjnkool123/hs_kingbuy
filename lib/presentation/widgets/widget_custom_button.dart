import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';

class CustomButton extends StatefulWidget {
  final String text;
  final Color color;
  final Color textColor;
  final Color borderSideColor;
  final Function onPressed;
  final double height;
  final double width;
  final double borderRadius;
  final double fontSize;

  const CustomButton(
      {Key key,
      this.color,
      this.textColor,
      this.text,
      this.onPressed,
      this.height,
      this.width,
      this.borderRadius,
      this.fontSize,
      this.borderSideColor})
      : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      height: widget.height ?? SizeConfig.blockSizeVertical * 7,
      minWidth: widget.width ?? SizeConfig.blockSizeHorizontal * 40,
      shape: RoundedRectangleBorder(
          side: BorderSide(color: widget.borderSideColor),
          borderRadius: BorderRadius.circular(widget.borderRadius)),
      color: widget.color,
      textColor: widget.textColor,
      padding: EdgeInsets.all(8.0),
      onPressed: (widget.onPressed),
      child: Text(
        widget.text ?? "",
        style: TextStyle(fontSize: widget.fontSize, fontFamily: 'RockWellStd'),
      ),
    );
  }
}
