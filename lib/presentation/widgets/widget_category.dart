import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';

class CategoriesView extends StatelessWidget {
  final Widget child;
  final String subTitle;

  const CategoriesView({Key key, this.subTitle, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        SizedBox(
          child: Container(
            width: 60,
            height: 60,
            child: Center(
              child: child,
            ),
          ),
        ),
        Text(subTitle ?? '', style: AppStyle.sub, textAlign: TextAlign.center,)
      ],
    );
  }
}
