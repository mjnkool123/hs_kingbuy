import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:intl/intl.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProductView extends StatefulWidget {
  final Widget child;
  final Function(dynamic) onTap;
  final String productName;
  final String brandName;
  final double rating;
  final int price;
  final int salePrice;
  final int saleOff;
  final int gifts;
  final int isInstallment;

  const ProductView(
      {Key key,
      this.child,
      this.onTap,
      this.productName,
      this.rating,
      this.price,
      this.salePrice,
      this.saleOff,
      this.brandName,
      this.isInstallment,
      this.gifts})
      : super(key: key);

  @override
  _ProductViewState createState() => _ProductViewState();
}

class _ProductViewState extends State<ProductView> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.safeBlockHorizontal * 50,
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 5.0)]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 160.0,
            child: Stack(
              children: <Widget>[
                Container(
                  child: widget.child,
                ),
                Positioned(
                  right: 0,
                  child: Container(
                    height: 25,
                    width: 50,
                    decoration: BoxDecoration(
                        color: Colors.red[600],
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Center(
                        child: Text(
                            "-" +
                                NumberFormat.decimalPattern()
                                    .format(widget.saleOff) +
                                "%",
                            style: TextStyle(color: Colors.white))),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: Text(
              widget.productName,
              style: AppStyle.productName,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.brandName,
                style: AppStyle.brandSub,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: SmoothStarRating(
                isReadOnly: true,
                size: 13,
                filledIconData: Icons.star,
                color: Colors.yellow[600],
                borderColor: Colors.grey.shade300,
                halfFilledIconData: Icons.star,
                defaultIconData: Icons.star,
                rating: widget.rating,
                starCount: 5,
                allowHalfRating: true,
                spacing: 1.0,
                onRated: (value) {
                  print("rating value -> $value");
                  // print("rating value dd -> ${value.truncate()}");
                },
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                NumberFormat.decimalPattern().format(widget.price) + "₫",
                style: AppStyle.price,
              ),
              Text(
                NumberFormat.decimalPattern().format(widget.salePrice) + "₫",
                style: AppStyle.priceSale,
              ),
            ],
          ),
          if(widget.gifts > 0)
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'assets/icons/gift_p2.png',
                  width: 25,
                  height: 25,
                ),
                Text(
                  "Quà tặng kèm",
                  style: AppStyle.brandSub,
                ),
              ],
            ))
          else
            Container(),
          if(widget.isInstallment > 0)
            Text("Trả góp 0%")
          else
            Text(""),
        ],
      ),
    );
  }
}
