import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class CustomTextFied extends StatefulWidget {
  final String hint;
  final bool isPassword;
  final bool isUserName;
  final bool showPassWord;
  final bool shadow;
  final FormFieldValidator validator;
  final TextEditingController controller;
  final MaskTextInputFormatter mask;
  final TextInputType type;
  final EdgeInsets padding;

  const CustomTextFied({
    Key key,
    @required this.hint,
    this.isPassword = false,
    this.validator,
    this.controller,
    this.mask,
    this.type,
    this.shadow = false,
    this.padding,
    this.isUserName = false,
    this.showPassWord = false,
  }) : super(key: key);

  @override
  _CustomTextFiedState createState() => _CustomTextFiedState();
}

class _CustomTextFiedState extends State<CustomTextFied> {
  var isShow;

  @override
  void initState() {
    isShow = widget.showPassWord ? true : false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: widget.shadow
              ? [
            BoxShadow(
                color: Colors.grey,
                offset: Offset(0, 0),
                spreadRadius: 0,
                blurRadius: 3)
          ]
              : [],
        ),
        child: Row(
          children: [
            Flexible(
              child: TextFormField(
                controller: widget.controller,
                validator: widget.validator,
                inputFormatters: widget.mask != null ? [widget.mask] : null,
                keyboardType: widget.type ?? TextInputType.text,
                obscureText: isShow,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  border: InputBorder.none,
                  hintText: widget.hint ?? 'null',
                  prefixIcon: widget.isUserName ? Icon(CupertinoIcons.person, color: Colors.grey,)
                      : widget.isPassword ? Icon(CupertinoIcons.lock, color: Colors.grey,) : null
                ),
              ),
            ),
            widget.showPassWord
                ? IconButton(
              onPressed: () {
                setState(() {
                  isShow = !isShow;
                });
              },
              icon: Icon(
                isShow ? CupertinoIcons.eye_slash : CupertinoIcons.eye,
              ),
              highlightColor: Colors.transparent,
            )
                : Container(),
          ],
        ),
      ),
    );
  }
}
