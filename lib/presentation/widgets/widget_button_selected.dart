import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';

class ButtonSelected extends StatefulWidget {
  final String text;
  final Function onTap;
  final bool isSelected;

  const ButtonSelected({Key key, this.text, this.onTap, this.isSelected})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ButtonSelectedState();
}

class _ButtonSelectedState extends State<ButtonSelected> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      child: Container(
        width: SizeConfig.safeBlockHorizontal * 30,
        height: 40,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: widget.isSelected
                    ? AssetImage("assets/images/bg_drawer_item_selected.png")
                    : AssetImage("assets/images/bg_drawer_item.png"),
                fit: BoxFit.fill)),
        child: Stack(
          children: [
            Center(child: Text(widget.text, textAlign: TextAlign.center,)),
            Positioned(
                right: 0,
                child: widget.isSelected
                    ? Image.asset("assets/icons/ic_check_button.png",
                        width: 10,
                        height: 10,
                        color: Color.fromRGBO(255, 255, 255, 1.0),
                        colorBlendMode: BlendMode.modulate)
                    : Image.asset("assets/icons/ic_check_button.png",
                        width: 10,
                        height: 10,
                        color: Color.fromRGBO(255, 255, 255, 0.0),
                        colorBlendMode: BlendMode.modulate))
          ],
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
