import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/configs/size_config.dart';
import 'package:hs_kingbuy/presentation/presentation.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: SizeConfig.blockSizeVertical * 30,
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: SizeConfig.safeBlockHorizontal * 40,
                  height: SizeConfig.safeBlockHorizontal * 40,
                  child: Image.asset('assets/images/logo.png')),
            ),
            Container(
              height: SizeConfig.blockSizeVertical * 45,
              alignment: Alignment.center,
              child: LoginForm(),
            ),
            Container(
              height: SizeConfig.blockSizeVertical * 10,
              alignment: Alignment.center,
            ),
            Container(
              height: SizeConfig.blockSizeVertical * 15,
              alignment: Alignment.center,
              child: Column(
                children: [
                  Text(
                    "Hoặc đăng nhập bằng",
                    style: AppStyle.sub,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                          icon: Image.asset('assets/icons/logo_face.png'),
                          onPressed: () {}),
                      IconButton(
                          icon: Image.asset('assets/icons/logo_google.png'),
                          onPressed: () {}),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Không có tài khoản ? ",
                        style: AppStyle.sub,
                      ),
                      InkWell(
                          onTap: () {},
                          child: Text("Đăng ký", style: AppStyle.subUnderline)),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Center(
              child: Text(
            "ĐĂNG NHẬP",
            style: AppStyle.title,
          )),
          SizedBox(
            height: SizeConfig.safeBlockHorizontal * 10,
          ),
          CustomTextFied(
            hint: "Email hoặc Số Điện Thoại",
            shadow: true,
            isUserName: true,
            type: TextInputType.name,
          ),
          CustomTextFied(
            hint: "Mật Khẩu",
            shadow: true,
            isPassword: true,
            type: TextInputType.visiblePassword,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () {},
                child: Text(
                  "Quên Mật Khẩu ?",
                  style: AppStyle.sub,
                ),
              ),
              CustomButton(
                borderRadius: 18.0,
                text: "Đăng Nhập",
                color: AppColors.colorPrimary,
                textColor: Colors.white,
                height: SizeConfig.safeBlockVertical * 7,
                width: SizeConfig.safeBlockHorizontal * 40,
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
