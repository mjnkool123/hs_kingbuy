import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/presentation/home/home.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentSelect = 0;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 8),
        child: Container(
          alignment: Alignment.bottomCenter,
          color: AppColors.colorPrimary,
          child: Row(children: [
            IconButton(
              iconSize: 50,
              icon: Image.asset('assets/images/logo.png'),
              onPressed: () {},
            ),
            Expanded(
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: TextField(
                  readOnly: true,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchScreen()));
                  },
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.search),
                    border: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.black26),
                    hintText: "Tìm kiếm",
                    contentPadding: EdgeInsets.all(12),
                  ),
                ),
              ),
            ),
            IconButton(
              iconSize: 28,
              color: Colors.white,
              icon: Icon(Icons.location_on_outlined),
              onPressed: () => null,
            ),
            IconButton(
              tooltip: "Giỏ hàng",
              icon: Image.asset(
                'assets/icons/shopping-cart.png',
                height: 28,
              ),
              onPressed: () => null,
            ),
          ]),
        ),
      ),
      body: IndexedStack(
        index: _currentSelect,
        children: <Widget>[
          MainHomeScreen(),
          Text(
            'Index 2: Danh mục',
          ),
          Text(
            'Index 3: Thẻ thành viên',
          ),
          Text(
            'Index 4: Thông báo',
          ),
          Text(
            'Index 5: Tài khoản',
          ),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey,
              blurRadius: 1,
            ),
          ],
        ),
        child: Theme(
          data: ThemeData(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                activeIcon: Image.asset(
                  'assets/icons/homeOn.png',
                  width: 25,
                  height: 25,
                ),
                icon: Image.asset(
                  'assets/icons/homeOff.png',
                  width: 25,
                  height: 25,
                ),
                label: 'Trang Chủ',
              ),
              BottomNavigationBarItem(
                activeIcon: Image.asset(
                  'assets/icons/menuOn.png',
                  width: 25,
                  height: 25,
                ),
                icon: Image.asset(
                  'assets/icons/menuOff.png',
                  width: 25,
                  height: 25,
                ),
                label: 'Danh mục',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.red, width: 4),
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 3),
                      color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Image.asset(
                        'assets/icons/card.png',
                        width: 30,
                        height: 30,
                      ),
                    ),
                  ),
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                activeIcon: Image.asset(
                  'assets/icons/alarmOn.png',
                  width: 25,
                  height: 25,
                ),
                icon: Image.asset(
                  'assets/icons/alarm.png',
                  width: 25,
                  height: 25,
                ),
                label: 'Thông báo',
              ),
              BottomNavigationBarItem(
                activeIcon: Image.asset(
                  'assets/icons/userOn.png',
                  width: 25,
                  height: 25,
                ),
                icon: Image.asset(
                  'assets/icons/userOff.png',
                  width: 25,
                  height: 25,
                ),
                label: 'Tài khoản',
              ),
            ],
            currentIndex: _currentSelect,
            selectedItemColor: AppColors.colorSecondary,
            onTap: (index) {
              setState(() {
                _currentSelect = index;
              });
            },
          ),
        ),
      ),
    );
  }
}
