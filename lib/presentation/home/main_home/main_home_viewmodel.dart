import 'package:flutter/cupertino.dart';
import 'package:hs_kingbuy/presentation/base/base.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:rxdart/rxdart.dart';

class MainHomeViewModel extends BaseViewModel {
  final PromotionRepository promotionRepository;
  final CategoryRepository categoryRepository;
  final ProductRepository allProductNewRepository;
  final ProductRepository productByCategoryRepository;

  final _pageController = BehaviorSubject<PageController>();

  var promotionSubject = BehaviorSubject<Promotion>();
  var myPromotionSubject = BehaviorSubject<MyPromotion>();
  var categorySubject = BehaviorSubject<Category>();
  var allProductNewSubject = BehaviorSubject<Product>();
  var massageChairSubject = BehaviorSubject<Product>();
  var treadMillSubject = BehaviorSubject<Product>();
  var sportSubject = BehaviorSubject<Product>();
  var massageMachineSubject = BehaviorSubject<Product>();
  var healthyBeautySubject = BehaviorSubject<Product>();
  var electronicSubject = BehaviorSubject<Product>();

  MainHomeViewModel(
      {this.promotionRepository,
      this.categoryRepository,
      this.allProductNewRepository,
      this.productByCategoryRepository});

  Future<void> init() async {
    final promotion = await promotionRepository.fetchPromotion(10, 0);
    final myPromotion = await promotionRepository.fetchMyPromotion(10, 0);
    final category = await categoryRepository.fetchCategory(15, 0);
    final allProductNew =
        await allProductNewRepository.fetchAllProductNew(20, 0);
    final messageChairProduct =
        await productByCategoryRepository.fetchProductByCategory(1);
    final treadmillProduct =
        await productByCategoryRepository.fetchProductByCategory(3);
    final sportProduct =
        await productByCategoryRepository.fetchProductByCategory(36);
    final massageMachineProduct =
        await productByCategoryRepository.fetchProductByCategory(4);
    final healthyBeautyProduct =
        await productByCategoryRepository.fetchProductByCategory(25);
    final electronicProduct =
        await productByCategoryRepository.fetchProductByCategory(31);

    pageController = PageController(initialPage: 0);


    promotionSubject.add(promotion);
    myPromotionSubject.add(myPromotion);
    categorySubject.add(category);
    allProductNewSubject.add(allProductNew);
    massageChairSubject.add(messageChairProduct);
    treadMillSubject.add(treadmillProduct);
    sportSubject.add(sportProduct);
    massageMachineSubject.add(massageMachineProduct);
    healthyBeautySubject.add(healthyBeautyProduct);
    electronicSubject.add(electronicProduct);
  }

  PageController get pageController => _pageController.value;

  set pageController(PageController value) {
    _pageController.value = value;
  }

  void backToHome() {
    pageController.jumpToPage(0);
  }

  void nextPage() {
    pageController
        .nextPage(duration: Duration(microseconds: 100), curve: Curves.ease);
  }

  void previousPage() {
    pageController.previousPage(
        duration: Duration(microseconds: 100), curve: Curves.ease);
  }

  Promotion get getPromotion => promotionSubject.value;

  MyPromotion get getMyPromotion => myPromotionSubject.value;

  Category get getCategories => categorySubject.value;

  Product get getAllProductNew => allProductNewSubject.value;

  Product get getMessageChairProduct => massageChairSubject.value;

  Product get getTreadmillProduct => treadMillSubject.value;

  Product get getSportProduct => sportSubject.value;

  Product get getMassageMachineProduct => massageMachineSubject.value;

  Product get getHealthyBeautyProduct => healthyBeautySubject.value;

  Product get getElectronicProduct => electronicSubject.value;


  @override
  void dispose() {
    myPromotionSubject.close();
    myPromotionSubject.drain();
    promotionSubject.close();
    promotionSubject.drain();
    categorySubject.close();
    categorySubject.drain();
    allProductNewSubject.close();
    allProductNewSubject.drain();
    massageChairSubject.close();
    massageChairSubject.drain();
    treadMillSubject.close();
    treadMillSubject.drain();
    sportSubject.close();
    sportSubject.drain();
    massageMachineSubject.close();
    massageMachineSubject.drain();
    healthyBeautySubject.close();
    healthyBeautySubject.drain();
    electronicSubject.close();
    electronicSubject.drain();
    _pageController.close();
    _pageController.drain();
    super.dispose();
  }
}
