import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';

import 'package:hs_kingbuy/presentation/presentation.dart';
import 'package:provider/provider.dart';

class MainHomeScreen extends StatelessWidget {
  const MainHomeScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SingleChildScrollView(
      child: BaseWidget<MainHomeViewModel>(
        viewModel: MainHomeViewModel(
          promotionRepository: PromotionRepository(),
          categoryRepository: CategoryRepository(),
          allProductNewRepository: ProductRepository(),
          productByCategoryRepository: ProductRepository(),
        ),
        onViewModelReady: (viewModel) async {
          viewModel.setLoading(true);
          await viewModel.init();
          viewModel.setLoading(false);
        },
        builder: (context, viewModel, child) {
          if (viewModel.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return MainHomeBody(
              promotion: viewModel.getPromotion,
              myPromotion: viewModel.getMyPromotion,
              category: viewModel.getCategories,
              product: viewModel.getAllProductNew,
              messageChairProduct: viewModel.getMessageChairProduct,
              treadmillProduct: viewModel.getTreadmillProduct,
              sportProduct: viewModel.getSportProduct,
              massageMachineProduct: viewModel.getMassageMachineProduct,
              healthyBeautyProduct: viewModel.getHealthyBeautyProduct,
              electronicProduct: viewModel.getElectronicProduct,
            );
          }
        },
      ),
    );
  }
}

class MainHomeBody extends StatelessWidget {
  final Promotion promotion;
  final MyPromotion myPromotion;
  final Category category;
  final Product product;
  final Product messageChairProduct;
  final Product treadmillProduct;
  final Product sportProduct;
  final Product massageMachineProduct;
  final Product healthyBeautyProduct;
  final Product electronicProduct;

  const MainHomeBody({
    Key key,
    this.promotion,
    this.myPromotion,
    this.category,
    this.product,
    this.messageChairProduct,
    this.treadmillProduct,
    this.sportProduct,
    this.massageMachineProduct,
    this.healthyBeautyProduct,
    this.electronicProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        WidgetPromotion(
          promotion: promotion,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Danh mục hot",
              style: AppStyle.title,
            ),
          ),
        ),
        HotCategory(
          category: category,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Ưu đãi dành cho bạn",
              style: AppStyle.title,
            ),
          ),
        ),
        WidgetMyPromotion(
          myPromotion: myPromotion,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Sản phẩm mới",
              style: AppStyle.title,
            ),
          ),
        ),
        ProductsNew(
          allProductNew: product,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Sản phẩm bán chạy",
              style: AppStyle.title,
            ),
          ),
        ),
        ProductsNew(
          allProductNew: product,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Ghế massage",
              style: AppStyle.title,
            ),
          ),
        ),
        MessageChair(
          messageChairProduct: messageChairProduct,
          category: category,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Máy chạy bộ",
              style: AppStyle.title,
            ),
          ),
        ),
        Treadmill(
          category: category,
          treadmillProduct: treadmillProduct,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Thể dục - thể thao",
              style: AppStyle.title,
            ),
          ),
        ),
        Sport(
          category: category,
          sportProduct: sportProduct,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Máy massage",
              style: AppStyle.title,
            ),
          ),
        ),
        MassageMachine(
          category: category,
          massageMachineProduct: massageMachineProduct,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Sức khỏe - sắc đẹp",
              style: AppStyle.title,
            ),
          ),
        ),
        HealthyBeauty(
          category: category,
          healthyBeautyProduct: healthyBeautyProduct,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Điện máy - gia dụng",
              style: AppStyle.title,
            ),
          ),
        ),
        Electronic(
          category: category,
          electronicProduct: electronicProduct,
        ),
      ],
    );
  }
}

class WidgetPromotion extends StatefulWidget {
  final Promotion promotion;

  const WidgetPromotion({Key key, this.promotion}) : super(key: key);

  @override
  _WidgetPromotionState createState() => _WidgetPromotionState();
}

class _WidgetPromotionState extends State<WidgetPromotion> {
  SwiperController controller;
  List<bool> autoplayes;
  List<SwiperController> controllers;

  @override
  void initState() {
    controller = new SwiperController();
    autoplayes = new List()
      ..length = 10
      ..fillRange(0, 10, false);
    controllers = new List()
      ..length = 10
      ..fillRange(0, 10, new SwiperController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.safeBlockVertical * 25,
      child: Swiper(
        autoplay: true,
        loop: true,
        itemCount: widget.promotion.data.news.length,
        controller: controller,
        pagination: SwiperPagination(
            margin: EdgeInsets.all(12), alignment: Alignment.bottomRight),
        itemBuilder: (BuildContext context, int index) {
          return Image.network(
            AppEndpoint.BASE_URL +
                widget.promotion.data.news[index].imageSource,
            fit: BoxFit.cover,
          );
        },
      ),
    );
  }
}

class HotCategory extends StatelessWidget {
  final Category category;

  const HotCategory({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: SizeConfig.blockSizeVertical * 30,
        child: GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              crossAxisSpacing: 4.0,
              mainAxisSpacing: 4.0,
            ),
            itemCount: category.data.categories.length,
            itemBuilder: (context, index) {
              return CategoriesView(
                child: Image.network(AppEndpoint.BASE_URL +
                    category.data.categories[index].imageSource),
                subTitle: category.data.categories[index].name,
              );
            }));
  }
}

class WidgetMyPromotion extends StatelessWidget {
  final MyPromotion myPromotion;

  const WidgetMyPromotion({Key key, this.myPromotion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: ListView.builder(
        itemCount: myPromotion.data.promotions.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.network(AppEndpoint.BASE_URL +
                  myPromotion.data.promotions[index].imageSource),
            ),
          );
        },
      ),
    );
  }
}

class ProductsNew extends StatelessWidget {
  final Product allProductNew;

  const ProductsNew({Key key, this.allProductNew}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.blockSizeVertical * 40,
      child: ListView.builder(
          itemCount: allProductNew.data.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductDetail(
                              item: allProductNew.data[index],
                            )));
              },
              child: ProductView(
                child: Image.network(AppEndpoint.BASE_URL +
                    allProductNew.data[index].imageSource),
                productName: allProductNew.data[index].name,
                brandName: allProductNew.data[index].brandName,
                rating: allProductNew.data[index].star,
                price: allProductNew.data[index].price,
                salePrice: allProductNew.data[index].salePrice,
                saleOff: allProductNew.data[index].saleOff,
                gifts: allProductNew.data[index].gifts.length,
                isInstallment: allProductNew.data[index].isInstallment,
              ),
            );
          }),
    );
  }
}

class MessageChair extends StatelessWidget {
  final Category category;
  final Product messageChairProduct;

  const MessageChair({Key key, this.category, this.messageChairProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<MainHomeViewModel>(context, listen: false);
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
              child: Image.network(AppEndpoint.BASE_URL +
                  category.data.categories[0].backgroundImage)),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[1].children.length, (index) {
                  return Text(
                    category.data.categories[0].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {
                  provider.nextPage();

                },
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                controller: provider.pageController,
                itemCount: category.data.categories[1].children.length,
                itemBuilder: (context, index) {
                  return ListView.builder(
                      itemCount: messageChairProduct.data.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductDetail(
                                          item: messageChairProduct.data[index],
                                        )));
                          },
                          child: ProductView(
                            child: Image.network(AppEndpoint.BASE_URL +
                                messageChairProduct.data[index].imageSource),
                            productName: messageChairProduct.data[index].name,
                            brandName:
                                messageChairProduct.data[index].brandName,
                            saleOff: messageChairProduct.data[index].saleOff,
                            rating: messageChairProduct.data[index].star,
                            price: messageChairProduct.data[index].price,
                            salePrice:
                                messageChairProduct.data[index].salePrice,
                            isInstallment:
                                messageChairProduct.data[index].isInstallment,
                            gifts: messageChairProduct.data[index].gifts.length,
                          ),
                        );
                      });
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Treadmill extends StatelessWidget {
  final Category category;
  final Product treadmillProduct;

  const Treadmill({Key key, this.category, this.treadmillProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            child: Image.network(AppEndpoint.BASE_URL +
                category.data.categories[1].backgroundImage),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[1].children.length, (index) {
                  return Text(
                    category.data.categories[1].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {},
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: ListView.builder(
                itemCount: treadmillProduct.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    item: treadmillProduct.data[index],
                                  )));
                    },
                    child: ProductView(
                      child: Image.network(AppEndpoint.BASE_URL +
                          treadmillProduct.data[index].imageSource),
                      productName: treadmillProduct.data[index].name,
                      brandName: treadmillProduct.data[index].brandName,
                      saleOff: treadmillProduct.data[index].saleOff,
                      rating: treadmillProduct.data[index].star,
                      price: treadmillProduct.data[index].price,
                      salePrice: treadmillProduct.data[index].salePrice,
                      isInstallment: treadmillProduct.data[index].isInstallment,
                      gifts: treadmillProduct.data[index].gifts.length,
                    ),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Sport extends StatelessWidget {
  final Category category;
  final Product sportProduct;

  const Sport({Key key, this.category, this.sportProduct}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            child: Image.network(AppEndpoint.BASE_URL +
                category.data.categories[2].backgroundImage),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[2].children.length, (index) {
                  return Text(
                    category.data.categories[2].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {},
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: ListView.builder(
                itemCount: sportProduct.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    item: sportProduct.data[index],
                                  )));
                    },
                    child: ProductView(
                      child: Image.network(AppEndpoint.BASE_URL +
                          sportProduct.data[index].imageSource),
                      productName: sportProduct.data[index].name,
                      brandName: sportProduct.data[index].brandName,
                      saleOff: sportProduct.data[index].saleOff,
                      rating: sportProduct.data[index].star,
                      price: sportProduct.data[index].price,
                      salePrice: sportProduct.data[index].salePrice,
                      isInstallment: sportProduct.data[index].isInstallment,
                      gifts: sportProduct.data[index].gifts.length,
                    ),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MassageMachine extends StatelessWidget {
  final Category category;
  final Product massageMachineProduct;

  const MassageMachine({Key key, this.category, this.massageMachineProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            child: Image.network(AppEndpoint.BASE_URL +
                category.data.categories[3].backgroundImage),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[3].children.length, (index) {
                  return Text(
                    category.data.categories[3].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {},
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: ListView.builder(
                itemCount: massageMachineProduct.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    item: massageMachineProduct.data[index],
                                  )));
                    },
                    child: ProductView(
                      child: Image.network(AppEndpoint.BASE_URL +
                          massageMachineProduct.data[index].imageSource),
                      productName: massageMachineProduct.data[index].name,
                      brandName: massageMachineProduct.data[index].brandName,
                      saleOff: massageMachineProduct.data[index].saleOff,
                      rating: massageMachineProduct.data[index].star,
                      price: massageMachineProduct.data[index].price,
                      salePrice: massageMachineProduct.data[index].salePrice,
                      isInstallment:
                          massageMachineProduct.data[index].isInstallment,
                      gifts: massageMachineProduct.data[index].gifts.length,
                    ),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}

class HealthyBeauty extends StatelessWidget {
  final Category category;
  final Product healthyBeautyProduct;

  const HealthyBeauty({Key key, this.category, this.healthyBeautyProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            child: Image.network(AppEndpoint.BASE_URL +
                category.data.categories[4].backgroundImage),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[4].children.length, (index) {
                  return Text(
                    category.data.categories[4].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {},
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: ListView.builder(
                itemCount: healthyBeautyProduct.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    item: healthyBeautyProduct.data[index],
                                  )));
                    },
                    child: ProductView(
                      child: Image.network(AppEndpoint.BASE_URL +
                          healthyBeautyProduct.data[index].imageSource),
                      productName: healthyBeautyProduct.data[index].name,
                      brandName: healthyBeautyProduct.data[index].brandName,
                      saleOff: healthyBeautyProduct.data[index].saleOff,
                      rating: healthyBeautyProduct.data[index].star,
                      price: healthyBeautyProduct.data[index].price,
                      salePrice: healthyBeautyProduct.data[index].salePrice,
                      isInstallment:
                          healthyBeautyProduct.data[index].isInstallment,
                      gifts: healthyBeautyProduct.data[index].gifts.length,
                    ),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Electronic extends StatelessWidget {
  final Category category;
  final Product electronicProduct;

  const Electronic({Key key, this.category, this.electronicProduct})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            child: Image.network(AppEndpoint.BASE_URL +
                category.data.categories[5].backgroundImage),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FilterSelect(
                defaultSelect: 0,
                children: List<Widget>.generate(
                    category.data.categories[5].children.length, (index) {
                  return Text(
                    category.data.categories[5].children[index].name,
                  );
                })
                  ..insert(0, Text("Tất cả")),
                onChanged: (value) {},
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                selectDecoration: BoxDecoration(
                  color: AppColors.colorPrimary,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                selectStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Container(
            height: 350,
            child: ListView.builder(
                itemCount: electronicProduct.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    item: electronicProduct.data[index],
                                  )));
                    },
                    child: ProductView(
                      child: Image.network(AppEndpoint.BASE_URL +
                          electronicProduct.data[index].imageSource),
                      productName: electronicProduct.data[index].name,
                      brandName: electronicProduct.data[index].brandName,
                      saleOff: electronicProduct.data[index].saleOff,
                      rating: electronicProduct.data[index].star,
                      price: electronicProduct.data[index].price,
                      salePrice: electronicProduct.data[index].salePrice,
                      isInstallment:
                          electronicProduct.data[index].isInstallment,
                      gifts: electronicProduct.data[index].gifts.length,
                    ),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: InkWell(
                child: Text(
                  "Xem thêm",
                  style: AppStyle.subMore,
                ),
                onTap: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}
