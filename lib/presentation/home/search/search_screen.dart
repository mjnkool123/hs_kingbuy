import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';

import 'package:hs_kingbuy/presentation/presentation.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  bool onSearching = true;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BaseWidget<SearchViewModel>(
        viewModel: SearchViewModel(searchRepository: SearchRepository()),
        onViewModelReady: (viewModel) async {
          viewModel.setLoading(true);
          viewModel.isLoading;
          viewModel.setLoading(false);
        },
        builder: (context, viewModel, child) {
          return Scaffold(
            key: _drawerKey,
            body: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 8, top: 30),
                  color: AppColors.colorPrimary,
                  height: SizeConfig.safeBlockVertical * 15,
                  child: Row(
                    children: [
                      Visibility(
                        visible: !onSearching,
                        child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Image.asset(
                            'assets/icons/left-arrow-white.png',
                            width: 20,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: TextField(
                            onSubmitted: (String keyword) {
                              setState(() {
                                onSearching = false;
                              });
                            },
                            onChanged: (keyword) {
                              viewModel.onSearching(keyword);
                            },
                            onTap: () {
                              setState(() {
                                onSearching = true;
                              });
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              border: InputBorder.none,
                              hintStyle: TextStyle(color: Colors.black26),
                              hintText: "Tìm kiếm",
                              contentPadding: EdgeInsets.all(12),
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: onSearching,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Hủy",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: !onSearching,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Column(
                              children: [
                                IconButton(
                                  tooltip: "Lọc",
                                  onPressed: () =>
                                      _drawerKey.currentState.openEndDrawer(),
                                  icon: Image.asset(
                                    'assets/icons/filter-white.png',
                                    width: 20,
                                  ),
                                ),
                                Text(
                                  "Lọc",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(12),
                  child: Stack(
                    children: [
                      Visibility(
                        visible: onSearching,
                        child: Column(
                          children: [
                            Container(
                              child: viewModel.isLoading
                                  ? CircularProgressIndicator()
                                  : viewModel.searchSubject.value == null
                                      ? Container()
                                      : Container(
                                          child: ListView.separated(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              separatorBuilder:
                                                  (BuildContext context,
                                                          int index) =>
                                                      Divider(),
                                              shrinkWrap: true,
                                              itemCount: viewModel
                                                  .getSearch.data.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: InkWell(
                                                    onTap: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  ProductDetail(
                                                                    item: viewModel
                                                                        .getSearch
                                                                        .data[index],
                                                                  )));
                                                      print(
                                                          "${viewModel.getSearch.data[index].name}");
                                                    },
                                                    child: RichText(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      text: TextSpan(
                                                        text: viewModel
                                                            .getSearch
                                                            .data[index]
                                                            .name,
                                                        style:
                                                            AppStyle.subDetail,
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              }),
                                        ),
                            ),
                            if (viewModel.searchSubject.value != null)
                              Text(
                                "Xóa lịch sử tìm kiếm",
                                style: AppStyle.sub,
                              ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: !onSearching,
                        child: viewModel.isLoading
                            ? CircularProgressIndicator()
                            : viewModel.searchSubject.value == null
                                ? Container()
                                : Container(
                                    height: SizeConfig.safeBlockVertical * 80,
                                    child: GridView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          childAspectRatio: 0.6,
                                          crossAxisSpacing: 10,
                                          mainAxisSpacing: 10,
                                        ),
                                        itemCount:
                                            viewModel.getSearch.data.length,
                                        itemBuilder: (context, index) {
                                          return ProductView(
                                            child: Image.network(
                                                AppEndpoint.BASE_URL +
                                                    viewModel
                                                        .getSearch
                                                        .data[index]
                                                        .imageSource),
                                            productName: viewModel
                                                .getSearch.data[index].name,
                                            brandName: viewModel.getSearch
                                                .data[index].brandName,
                                            rating: viewModel
                                                .getSearch.data[index].star,
                                            price: viewModel
                                                .getSearch.data[index].price,
                                            salePrice: viewModel.getSearch
                                                .data[index].salePrice,
                                            saleOff: viewModel
                                                .getSearch.data[index].saleOff,
                                            gifts: viewModel.getSearch
                                                .data[index].gifts.length,
                                            isInstallment: viewModel.getSearch
                                                .data[index].isInstallment,
                                          );
                                        }),
                                  ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            // endDrawerEnableOpenDragGesture: false,
            endDrawer: Drawer(
              child: Scaffold(
                appBar: AppBar(
                  elevation: 1.0,
                  leading: IconButton(
                    tooltip: "Lọc",
                    onPressed: () {},
                    icon: Image.asset(
                      'assets/icons/filter-white.png',
                      width: 20,
                    ),
                  ),
                  title: Text("Bộ lọc tìm kiếm", style: AppStyle.subWhite),
                  backgroundColor: AppColors.colorPrimary,
                ),
                body: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Danh mục", style: AppStyle.title)),
                          SizedBox(height: 10),
                          Container(
                              height: SizeConfig.blockSizeVertical * 30,
                              child: GridView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 3,
                                    crossAxisSpacing: 5,
                                    mainAxisSpacing: 5,
                                  ),
                                  itemCount: viewModel.getCategories().length,
                                  itemBuilder: (context, index) {
                                    return ButtonSelected(
                                      onTap: () {
                                        viewModel.selectedCategory =
                                            viewModel.getCategories()[index].id;
                                      },
                                      text:
                                          viewModel.getCategories()[index].name,
                                      isSelected: viewModel.selectedCategory ==
                                          viewModel.getCategories()[index].id,
                                    );
                                  })),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child:
                                  Text("Thương hiệu", style: AppStyle.title)),
                          SizedBox(height: 10),
                          Container(
                              height: SizeConfig.blockSizeVertical * 30,
                              child: GridView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 3,
                                    crossAxisSpacing: 5,
                                    mainAxisSpacing: 5,
                                  ),
                                  itemCount: viewModel.getBrands().length,
                                  itemBuilder: (context, index) {
                                    return ButtonSelected(
                                      onTap: () {
                                        viewModel.selectedBrand = viewModel
                                            .getBrands()[index]
                                            .brandId;
                                      },
                                      text: viewModel
                                          .getBrands()[index]
                                          .brandName,
                                      isSelected: viewModel.selectedBrand ==
                                          viewModel.getBrands()[index].brandId,
                                    );
                                  })),
                        ],
                      ),
                    )
                  ],
                ),
                bottomNavigationBar: BottomAppBar(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CustomButton(
                        borderSideColor: Colors.grey.shade400,
                        borderRadius: 18,
                        text: "Thiết lập lại",
                        fontSize: 15,
                        color: Colors.grey.shade400,
                        textColor: Colors.white,
                        height: SizeConfig.safeBlockVertical * 5,
                        width: SizeConfig.safeBlockHorizontal * 25,
                        onPressed: () {},
                      ),
                      CustomButton(
                        borderSideColor: AppColors.colorSecondary,
                        borderRadius: 18,
                        text: "Áp dụng",
                        fontSize: 15,
                        color: AppColors.colorSecondary,
                        textColor: Colors.white,
                        height: SizeConfig.safeBlockVertical * 5,
                        width: SizeConfig.safeBlockHorizontal * 25,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
