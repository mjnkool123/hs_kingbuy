import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/presentation/presentation.dart';
import 'package:rxdart/rxdart.dart';

class SearchViewModel extends BaseViewModel {
  final SearchRepository searchRepository;

  var searchSubject = BehaviorSubject<Product>();
  var categorySubject = BehaviorSubject<CategoryOfProduct>();
  var brandSubject = BehaviorSubject<DatumProduct>();

  final _searchKeyWord = BehaviorSubject<String>();
  final _selectedCategory = BehaviorSubject<int>();
  final _selectedBrand = BehaviorSubject<int>();
  final _selectedPrice = BehaviorSubject<int>();

  SearchViewModel({this.searchRepository});

  void onSearching(String keyword) async {
    setLoading(true);
    searchSubject.add(await searchRepository.fetchSearch(keyword));
    setLoading(false);
    notifyListeners();
  }

  List<CategoryOfProduct> getCategories() {
    final list = <CategoryOfProduct>[];
    if(searchSubject.value == null) return [];
    searchSubject.value.data.forEach((element) {
      if(list.where((item) => item.id == element.category.id).length == 0) {
        list.add(element.category);
      }
    });
    return list;
  }

  List<DatumProduct> getBrands() {
    final list = <DatumProduct>[];
    if(searchSubject.value == null) return [];
    searchSubject.value.data.forEach((element) {
      if(list.where((item) => item.brandId == element.brandId).length == 0) {
        list.add(element);
      }
    });
    return list;
  }

  // CategoryOfProduct get getCategories => categorySubject.value;

  Product get getSearch => searchSubject.value;

  String get searchKeyWord => _searchKeyWord.value;

  set searchKeyWord(value) {
    _searchKeyWord.add(value);
    notifyListeners();
  }

  int get selectedCategory => _selectedCategory.value;

  set selectedCategory(value) {
    _selectedCategory.add(value);
    notifyListeners();
  }

  int get selectedBrand => _selectedBrand.value;

  set selectedBrand(value) {
    _selectedBrand.add(value);
    notifyListeners();
  }

  int get selectedPrice => _selectedPrice.value;

  set selectedPrice(value) {
    _selectedPrice.add(value);
  }



  @override
  void dispose() {
    _searchKeyWord.close();
    _searchKeyWord.drain();
    searchSubject.close();
    searchSubject.drain();
    _selectedCategory.close();
    _selectedCategory.drain();
    _selectedBrand.close();
    _selectedBrand.drain();
    _selectedPrice.close();
    _selectedPrice.drain();
    categorySubject.close();
    categorySubject.drain();
    brandSubject.close();
    brandSubject.drain();
    super.dispose();
  }
}
