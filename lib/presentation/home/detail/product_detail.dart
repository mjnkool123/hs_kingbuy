import 'dart:ui';

import 'package:expand_widget/expand_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/presentation/presentation.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:video_player/video_player.dart';

class ProductDetail extends StatelessWidget {
  final DatumProduct item;

  const ProductDetail({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 8),
        child: Container(
          alignment: Alignment.bottomCenter,
          color: AppColors.colorPrimary,
          padding: EdgeInsets.all(5),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              iconSize: 30,
              color: Colors.white,
              icon: Icon(Icons.arrow_back_outlined),
            ),
            Row(
              children: [
                IconButton(
                  iconSize: 30,
                  color: Colors.white,
                  icon: Icon(Icons.search),
                  onPressed: () => null,
                ),
                IconButton(
                  iconSize: 30,
                  color: Colors.white,
                  icon: Icon(Icons.home_outlined),
                  onPressed: () => null,
                ),
                IconButton(
                  icon: Image.asset(
                    'assets/icons/shopping-cart.png',
                    height: 25,
                    width: 25,
                  ),
                  onPressed: () => null,
                ),
              ],
            ),
          ]),
        ),
      ),
      body: SingleChildScrollView(
        child: BaseWidget<DetailViewModel>(
          viewModel: DetailViewModel(
            productRepository: ProductRepository(),
            addressRepository: AddressRepository(),
            productId: item.id,
          ),
          onViewModelReady: (viewModel) async {
            viewModel.setLoading(true);
            await viewModel.init();
            viewModel.setLoading(false);
          },
          builder: (context, viewModel, child) {
            if (viewModel.isLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return DetailBody(
                item: item,
                address: viewModel.getAddress,
                productRatingInfo: viewModel.getRatingInfo,
                productReview: viewModel.getReviewProduct,
                productQuestion: viewModel.getProductQuestion,
                purchasedTogetherProduct: viewModel.getPurchasedTogetherProduct,
                relatedProduct: viewModel.getRelatedProduct,
              );
            }
          },
        ),
      ),
    );
  }
}

class DetailBody extends StatefulWidget {
  final DatumProduct item;
  final Address address;
  final ProductRatingInfo productRatingInfo;
  final ProductReview productReview;
  final ProductQuestion productQuestion;
  final PurchasedTogetherProduct purchasedTogetherProduct;
  final RelatedProduct relatedProduct;

  const DetailBody(
      {Key key,
      this.item,
      this.address,
      this.productRatingInfo,
      this.productReview,
      this.productQuestion,
      this.purchasedTogetherProduct,
      this.relatedProduct})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DetailBodyState();
}

class _DetailBodyState extends State<DetailBody> {
  SwiperController controller;
  VideoPlayerController _vidController;

  List<bool> autoplayes;
  List<SwiperController> controllers;

  @override
  void initState() {
    if (widget.item.videoLink != null) {
      _vidController = VideoPlayerController.network(widget.item.videoLink)
        ..addListener(() {
          if (_vidController.value.initialized) {
            print(_vidController.value.position);
          }
        })
        ..initialize().then((value) => setState(() {}));
      controller = new SwiperController();
      autoplayes = new List()
        ..length = 10
        ..fillRange(0, 10, false);
      controllers = new List()
        ..length = 10
        ..fillRange(0, 10, new SwiperController());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: [
              Container(
                height: SizeConfig.safeBlockVertical * 50,
                child: Swiper(
                  loop: true,
                  itemCount: widget.item.imageSourceList.length,
                  controller: controller,
                  pagination: SwiperPagination(),
                  itemBuilder: (BuildContext context, int index) {
                    return Image.network(
                      AppEndpoint.BASE_URL + widget.item.imageSourceList[index],
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 8),
                    child: Text(
                      widget.item.name,
                      style: AppStyle.title,
                    ),
                  )),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    SmoothStarRating(
                      rating: widget.item.star,
                      isReadOnly: true,
                      size: 24,
                      filledIconData: Icons.star,
                      color: Colors.yellow[600],
                      borderColor: Colors.grey.shade300,
                      halfFilledIconData: Icons.star,
                      defaultIconData: Icons.star,
                      starCount: 5,
                      allowHalfRating: false,
                      spacing: 2.0,
                      onRated: (value) {
                        print("rating value -> $value");
                        // print("rating value dd -> ${value.truncate()}");
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    if (widget.item.star == 0)
                      Text(
                        "(Chưa có nhận xét nào)",
                        style: AppStyle.detailComment,
                      )
                    else
                      Text(
                        "(Xem nhận xét)",
                        style: AppStyle.detailComment,
                      )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        NumberFormat.decimalPattern()
                                .format(widget.item.salePrice) +
                            "₫",
                        style: AppStyle.priceDetail,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        NumberFormat.decimalPattern()
                                .format(widget.item.price) +
                            "₫",
                        style: AppStyle.priceSaleDetail,
                      ),
                    ],
                  ),
                  Container(
                    height: 30,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Colors.red[600],
                        borderRadius: BorderRadius.circular(14.0)),
                    child: Center(
                        child: Text(
                            "-" +
                                NumberFormat.decimalPattern()
                                    .format(widget.item.saleOff) +
                                "%",
                            style: TextStyle(color: Colors.white))),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Text(
                      "Tình trạng: ",
                      style: AppStyle.subDetail,
                    ),
                    if (widget.item.status == 0)
                      Text(
                        "Hết hàng",
                        style: AppStyle.sttOutOfStocking,
                      )
                    else
                      Text(
                        "Còn hàng",
                        style: AppStyle.sttStocking,
                      )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              CustomButton(
                borderSideColor: AppColors.colorThirdly,
                borderRadius: 5.0,
                text: "Đặt mua trả góp",
                fontSize: 15,
                color: AppColors.colorThirdly,
                textColor: Colors.white,
                height: SizeConfig.safeBlockVertical * 5,
                width: SizeConfig.safeBlockHorizontal * 50,
                onPressed: () {},
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        if (widget.item.gifts.length > 0)
          Container(
            padding: EdgeInsets.all(8.0),
            color: Colors.white,
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(
                      'assets/icons/gift_p.png',
                      height: 30,
                      width: 30,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Quà tặng kèm: ", style: AppStyle.detailTxtGift)
                  ],
                ),
                ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.item.gifts.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: Container(
                        height: SizeConfig.safeBlockVertical * 10,
                        width: SizeConfig.safeBlockHorizontal * 14,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(AppEndpoint.BASE_URL +
                                  widget.item.gifts[index].imageSource),
                            ),
                            border: Border.all(color: Colors.blueAccent)),
                        // child: Image.network(AppEndpoint.BASE_URL +
                        //     item.gifts[index].imageSource),
                      ),
                      title: Text(widget.item.gifts[index].name),
                      subtitle: Text(
                        NumberFormat.decimalPattern()
                                .format(widget.item.gifts[index].price) +
                            "₫",
                        style: AppStyle.price,
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Hệ thống Kingbuy",
                    style: AppStyle.title,
                  )),
              ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Row(children: [
                        Image.asset(
                          'assets/icons/pin.png',
                          height: 25,
                        ),
                        Text(
                          " KingBuy",
                          style: AppStyle.priceDetail,
                        ),
                      ]),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            widget.address.data[index].address,
                            style: AppStyle.subDetail,
                          ),
                          Row(
                            children: [
                              Image.asset(
                                'assets/icons/ic_phone.png',
                                height: 14,
                              ),
                              Text(
                                " " + widget.address.data[index].hotLine,
                                style: AppStyle.phoneNumber,
                              )
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemCount: widget.address.data.length)
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Thông tin sản phẩm',
                  style: AppStyle.title,
                ),
              ),
              SizedBox(height: 8),
              ExpandChild(
                arrowSize: 40,
                expandArrowStyle: ExpandArrowStyle.both,
                expandedHint: "Ẩn bớt",
                collapsedHint: "Xem tất cả",
                hintTextStyle: AppStyle.sub,
                capitalArrowtext: true,
                child: Html(
                  style: {
                    "html": Style(
                        fontSize: FontSize(18), textAlign: TextAlign.justify),
                  },
                  data: widget.item.content,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Thông số kỹ thuật',
                  style: AppStyle.title,
                ),
              ),
              SizedBox(height: 8),
              Container(
                child: Html(style: {
                  "html": Style(
                      fontSize: FontSize(18), textAlign: TextAlign.justify),
                }, data: widget.item.specifications ?? ""),
              )
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Video',
                  style: AppStyle.title,
                ),
              ),
              SizedBox(height: 8),
              Container(
                child: widget.item.videoLink == null
                    ? Container()
                    : _vidController.value.initialized
                        ? AspectRatio(
                            aspectRatio: _vidController.value.aspectRatio,
                            child: VideoPlayer(_vidController),
                          )
                        : Container(),
              )
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Thông tin thương hiệu',
                  style: AppStyle.title,
                ),
              ),
              SizedBox(height: 8),
              ExpandChild(
                arrowSize: 40,
                expandArrowStyle: ExpandArrowStyle.both,
                expandedHint: "Ẩn bớt",
                collapsedHint: "Xem tất cả",
                hintTextStyle: AppStyle.sub,
                capitalArrowtext: true,
                child: Html(
                  style: {
                    "html": Style(
                        fontSize: FontSize(18), textAlign: TextAlign.justify),
                  },
                  data: widget.item.brandInfo ?? "",
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Đánh giá & Bình luận',
                  style: AppStyle.title,
                ),
              ),
              SizedBox(height: 8),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Column(
                          children: [
                            Text(
                              "Đánh giá trung bình",
                              style: AppStyle.sub,
                            ),
                            Text(
                                "( ${widget.productRatingInfo.data.ratingCount} đánh giá)",
                                style: AppStyle.subDetail),
                            Text(
                              "${widget.productRatingInfo.data.avgRating.toDouble()}/5",
                              style: AppStyle.title,
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "5",
                                  style: AppStyle.subDetail,
                                ),
                                Icon(
                                  Icons.star,
                                  color: Colors.grey,
                                  size: 15,
                                ),
                                LinearPercentIndicator(
                                  width: 80,
                                  lineHeight: 8.0,
                                  percent: (widget.productRatingInfo.data
                                                  .fiveStarCount ==
                                              0
                                          ? 0
                                          : widget.productRatingInfo.data
                                                  .fiveStarCount *
                                              100 /
                                              widget.productRatingInfo.data
                                                  .ratingCount) /
                                      100,
                                  progressColor: Colors.yellow[600],
                                ),
                                Expanded(
                                    child: Text(
                                        "${widget.productRatingInfo.data.fiveStarCount} đánh ...",
                                        style: AppStyle.subDetail)),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "4",
                                  style: AppStyle.subDetail,
                                ),
                                Icon(
                                  Icons.star,
                                  color: Colors.grey,
                                  size: 15,
                                ),
                                LinearPercentIndicator(
                                  width: 80,
                                  lineHeight: 8.0,
                                  percent: (widget.productRatingInfo.data
                                                  .fourStarCount ==
                                              0
                                          ? 0
                                          : widget.productRatingInfo.data
                                                  .fourStarCount *
                                              100 /
                                              widget.productRatingInfo.data
                                                  .ratingCount) /
                                      100,
                                  progressColor: Colors.yellow[600],
                                ),
                                Expanded(
                                    child: Text(
                                        "${widget.productRatingInfo.data.fourStarCount} đánh ...",
                                        style: AppStyle.subDetail)),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "3",
                                  style: AppStyle.subDetail,
                                ),
                                Icon(
                                  Icons.star,
                                  color: Colors.grey,
                                  size: 15,
                                ),
                                LinearPercentIndicator(
                                  width: 80,
                                  lineHeight: 8.0,
                                  percent: (widget.productRatingInfo.data
                                                  .threeStarCount ==
                                              0
                                          ? 0
                                          : widget.productRatingInfo.data
                                                  .threeStarCount *
                                              100 /
                                              widget.productRatingInfo.data
                                                  .ratingCount) /
                                      100,
                                  progressColor: Colors.yellow[600],
                                ),
                                Expanded(
                                    child: Text(
                                        "${widget.productRatingInfo.data.threeStarCount} đánh ...",
                                        style: AppStyle.subDetail)),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "2",
                                  style: AppStyle.subDetail,
                                ),
                                Icon(
                                  Icons.star,
                                  color: Colors.grey,
                                  size: 15,
                                ),
                                LinearPercentIndicator(
                                  width: 80,
                                  lineHeight: 8.0,
                                  percent: (widget.productRatingInfo.data
                                                  .twoStarCount ==
                                              0
                                          ? 0
                                          : widget.productRatingInfo.data
                                                  .twoStarCount *
                                              100 /
                                              widget.productRatingInfo.data
                                                  .ratingCount) /
                                      100,
                                  progressColor: Colors.yellow[600],
                                ),
                                Expanded(
                                    child: Text(
                                        "${widget.productRatingInfo.data.twoStarCount} đánh ...",
                                        style: AppStyle.subDetail)),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "1",
                                  style: AppStyle.subDetail,
                                ),
                                Icon(
                                  Icons.star,
                                  color: Colors.grey,
                                  size: 15,
                                ),
                                LinearPercentIndicator(
                                  width: 80,
                                  lineHeight: 8.0,
                                  percent: (widget.productRatingInfo.data
                                                  .oneStarCount ==
                                              0
                                          ? 0
                                          : widget.productRatingInfo.data
                                                  .oneStarCount *
                                              100 /
                                              widget.productRatingInfo.data
                                                  .ratingCount) /
                                      100,
                                  progressColor: Colors.yellow[600],
                                ),
                                Expanded(
                                    child: Text(
                                        "${widget.productRatingInfo.data.oneStarCount} đánh ...",
                                        style: AppStyle.subDetail)),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  CustomButton(
                    borderSideColor: AppColors.colorSecondary,
                    borderRadius: 5.0,
                    text: "Viết đánh giá",
                    fontSize: 15,
                    color: Colors.white,
                    textColor: Colors.red,
                    height: SizeConfig.safeBlockVertical * 5,
                    width: SizeConfig.safeBlockHorizontal * 80,
                    onPressed: () {},
                  ),
                  Divider(
                    color: Colors.black38,
                  ),
                  ListView.separated(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          contentPadding: EdgeInsets.zero,
                          leading: ClipRRect(
                            borderRadius: BorderRadius.circular(30),
                            child: Image.network(
                              AppEndpoint.BASE_URL +
                                  widget.productReview.data.rows[index]
                                      .avatarSource,
                              height: 40,
                              width: 40,
                              fit: BoxFit.fill,
                            ),
                          ),
                          title: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.productReview.data.rows[index].name,
                                    style: AppStyle.sub,
                                  ),
                                  SmoothStarRating(
                                    rating: widget
                                        .productReview.data.rows[index].star,
                                    isReadOnly: true,
                                    size: 12,
                                    filledIconData: Icons.star,
                                    color: Colors.yellow[600],
                                    borderColor: Colors.grey.shade300,
                                    halfFilledIconData: Icons.star,
                                    defaultIconData: Icons.star,
                                    starCount: 5,
                                    allowHalfRating: false,
                                    spacing: 2.0,
                                    onRated: (value) {
                                      print("rating value -> $value");
                                      // print("rating value dd -> ${value.truncate()}");
                                    },
                                  ),
                                ],
                              ),
                              Text(
                                widget
                                    .productReview.data.rows[index].phoneNumber,
                                style: TextStyle(
                                    color: Colors.grey.withOpacity(0.8)),
                              ),
                            ],
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.productReview.data.rows[index].comment,
                                style: AppStyle.subDetail,
                              ),
                              Text(
                                "${DateFormat("yyyy-MM-dd").format(widget.productReview.data.rows[index].updatedAt)}",
                                style: TextStyle(color: Colors.black54),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                            color: Colors.black38,
                          ),
                      itemCount: widget.productReview.data.rows.length)
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Hỏi đáp về sản phẩm",
                    style: AppStyle.title,
                  ),
                  InkWell(
                      onTap: () {},
                      child: Icon(
                        Icons.arrow_forward_ios,
                        size: 25,
                      ))
                ],
              ),
              if (widget.productQuestion.data.rows.length > 0)
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "...",
                      style: AppStyle.sub,
                    )),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Chưa có câu hỏi nào!",
                    style: TextStyle(color: Colors.red)),
              ),
              CustomButton(
                borderSideColor: AppColors.colorSecondary,
                borderRadius: 20,
                text: "Đặt câu hỏi cho sản phẩm",
                fontSize: 16,
                color: AppColors.colorSecondary,
                textColor: Colors.white,
                height: SizeConfig.safeBlockVertical * 5,
                width: SizeConfig.safeBlockHorizontal * 60,
                onPressed: () {},
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          color: Colors.white,
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Thường được mua cùng", style: AppStyle.title)),
              ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      contentPadding: EdgeInsets.zero,
                      leading: Image.network(
                        AppEndpoint.BASE_URL +
                            widget.purchasedTogetherProduct.data.products[index]
                                .imageSource,
                        height: 40,
                        width: 40,
                        fit: BoxFit.fill,
                      ),
                      title: Text(
                        widget
                            .purchasedTogetherProduct.data.products[index].name,
                        style: AppStyle.subDetail,
                      ),
                      subtitle: Text(
                        NumberFormat.decimalPattern().format(widget
                                .purchasedTogetherProduct
                                .data
                                .products[index]
                                .price) +
                            "đ",
                        style: AppStyle.sub,
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemCount:
                      widget.purchasedTogetherProduct.data.products.length),
              CustomButton(
                borderSideColor: AppColors.colorSecondary,
                borderRadius: 20,
                text: "Thêm sản phẩm vào giỏ hàng",
                fontSize: 16,
                color: AppColors.colorSecondary,
                textColor: Colors.white,
                height: SizeConfig.safeBlockVertical * 5,
                width: SizeConfig.safeBlockHorizontal * 80,
                onPressed: () {},
              ),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          color: Colors.white,
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Sản phẩm liên quan", style: AppStyle.title)),
              Container(
                height: SizeConfig.blockSizeVertical * 40,
                child: ListView.builder(
                    itemCount: widget.relatedProduct.data.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {},
                        child: ProductView(
                          child: Image.network(AppEndpoint.BASE_URL +
                              widget.relatedProduct.data[index].imageSource),
                          productName: widget.relatedProduct.data[index].name,
                          brandName:
                              widget.relatedProduct.data[index].brandName,
                          rating: widget.relatedProduct.data[index].star,
                          price: widget.relatedProduct.data[index].price,
                          salePrice:
                              widget.relatedProduct.data[index].salePrice,
                          saleOff: widget.relatedProduct.data[index].saleOff,
                          gifts: widget.relatedProduct.data[index].gifts.length,
                          isInstallment: widget.relatedProduct.data[index].isInstallment,
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          color: Colors.white,
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Sản phẩm đã xem", style: AppStyle.title)),
            ],
          ),
        ),
      ],
    );
  }
}
