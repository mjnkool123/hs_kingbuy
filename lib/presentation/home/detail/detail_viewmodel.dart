import 'package:hs_kingbuy/presentation/base/base.dart';
import 'package:hs_kingbuy/resource/models/product_rating.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:rxdart/rxdart.dart';

class DetailViewModel extends BaseViewModel {
  final AddressRepository addressRepository;
  final ProductRepository productRepository;
  final int productId;

  var addressSubject = BehaviorSubject<Address>();
  var ratingInfoSubject = BehaviorSubject<ProductRatingInfo>();
  var reviewProductSubject = BehaviorSubject<ProductReview>();
  var productQuestionSubject = BehaviorSubject<ProductQuestion>();
  var purChasedTogetherProductSubject = BehaviorSubject<PurchasedTogetherProduct>();
  var relatedProductSubject = BehaviorSubject<RelatedProduct>();

  DetailViewModel({this.addressRepository, this.productRepository, this.productId});

  Future<void> init() async {
    final address = await addressRepository.fetchAddress();
    final ratingInfo = await productRepository.fetchRatingInfo(productId);
    final reviewProduct = await productRepository.fetchReviewProduct(productId);
    final productQuestion = await productRepository.fetchProductQuestion(productId);
    final purChasedTogetherProduct = await productRepository.fetchPurchasedTogetherProduct(productId, 10, 0);
    final relatedProduct = await productRepository.fetchRelatedProduct(productId, 10, 0);

    addressSubject.add(address);
    ratingInfoSubject.add(ratingInfo);
    reviewProductSubject.add(reviewProduct);
    productQuestionSubject.add(productQuestion);
    purChasedTogetherProductSubject.add(purChasedTogetherProduct);
    relatedProductSubject.add(relatedProduct);
  }

  Address get getAddress => addressSubject.value;
  ProductRatingInfo get getRatingInfo => ratingInfoSubject.value;
  ProductReview get getReviewProduct => reviewProductSubject.value;
  ProductQuestion get getProductQuestion => productQuestionSubject.value;
  PurchasedTogetherProduct get getPurchasedTogetherProduct => purChasedTogetherProductSubject.value;
  RelatedProduct get getRelatedProduct => relatedProductSubject.value;

  @override
  void dispose() {
    addressSubject.close();
    addressSubject.drain();
    ratingInfoSubject.close();
    ratingInfoSubject.drain();
    reviewProductSubject.close();
    reviewProductSubject.drain();
    productQuestionSubject.close();
    productQuestionSubject.drain();
    purChasedTogetherProductSubject.close();
    purChasedTogetherProductSubject.drain();
    relatedProductSubject.close();
    relatedProductSubject.drain();
    super.dispose();
  }
}
