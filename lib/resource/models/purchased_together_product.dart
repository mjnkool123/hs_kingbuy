import 'dart:convert';

class PurchasedTogetherProduct {
  PurchasedTogetherProduct({
    this.status,
    this.data,
  });

  final int status;
  final DataPurchasedTogetherProduct data;

  factory PurchasedTogetherProduct.fromRawJson(String str) =>
      PurchasedTogetherProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PurchasedTogetherProduct.fromJson(Map<String, dynamic> json) =>
      PurchasedTogetherProduct(
        status: json["status"],
        data: DataPurchasedTogetherProduct.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class DataPurchasedTogetherProduct {
  DataPurchasedTogetherProduct({
    this.products,
    this.recordsTotal,
  });

  final List<ProductTogether> products;
  final int recordsTotal;

  factory DataPurchasedTogetherProduct.fromRawJson(String str) => DataPurchasedTogetherProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataPurchasedTogetherProduct.fromJson(Map<String, dynamic> json) => DataPurchasedTogetherProduct(
        products: List<ProductTogether>.from(
            json["products"].map((x) => ProductTogether.fromJson(x))),
        recordsTotal: json["recordsTotal"],
      );

  Map<String, dynamic> toJson() => {
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
        "recordsTotal": recordsTotal,
      };
}

class ProductTogether {
  ProductTogether({
    this.id,
    this.name,
    this.price,
    this.salePrice,
    this.productCategoryId,
    this.category,
    this.brandId,
    this.giftIds,
    this.imageSource,
    this.imageSourceList,
    this.description,
    this.content,
    this.specifications,
    this.videoLink,
    this.slug,
    this.status,
    this.goodsStatus,
    this.isBulky,
    this.isInstallment,
    this.barcode,
    this.createdAt,
    this.updatedAt,
    this.productCategoryName,
    this.brandName,
    this.brandInfo,
    this.saleOff,
    this.gifts,
    this.star,
    this.colors,
  });

  final int id;
  final String name;
  final int price;
  final int salePrice;
  final int productCategoryId;
  final CategoryOfProductTogether category;
  final int brandId;
  final List<dynamic> giftIds;
  final String imageSource;
  final List<String> imageSourceList;
  final String description;
  final String content;
  final String specifications;
  final dynamic videoLink;
  final String slug;
  final int status;
  final int goodsStatus;
  final int isBulky;
  final int isInstallment;
  final String barcode;
  final String createdAt;
  final String updatedAt;
  final String productCategoryName;
  final String brandName;
  final dynamic brandInfo;
  final int saleOff;
  final List<dynamic> gifts;
  final int star;
  final List<ColorOfProductTogether> colors;

  factory ProductTogether.fromRawJson(String str) => ProductTogether.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductTogether.fromJson(Map<String, dynamic> json) => ProductTogether(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        salePrice: json["sale_price"],
        productCategoryId: json["product_category_id"],
        category: CategoryOfProductTogether.fromJson(json["category"]),
        brandId: json["brand_id"],
        giftIds: List<dynamic>.from(json["gift_ids"].map((x) => x)),
        imageSource: json["image_source"],
        imageSourceList:
            List<String>.from(json["image_source_list"].map((x) => x)),
        description: json["description"],
        content: json["content"],
        specifications: json["specifications"],
        videoLink: json["video_link"],
        slug: json["slug"],
        status: json["status"],
        goodsStatus: json["goods_status"],
        isBulky: json["is_bulky"],
        isInstallment: json["is_installment"],
        barcode: json["barcode"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        productCategoryName: json["product_category_name"],
        brandName: json["brand_name"],
        brandInfo: json["brand_info"],
        saleOff: json["sale_off"],
        gifts: List<dynamic>.from(json["gifts"].map((x) => x)),
        star: json["star"],
        colors: List<ColorOfProductTogether>.from(json["colors"].map((x) => ColorOfProductTogether.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "sale_price": salePrice,
        "product_category_id": productCategoryId,
        "category": category.toJson(),
        "brand_id": brandId,
        "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
        "image_source": imageSource,
        "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
        "description": description,
        "content": content,
        "specifications": specifications,
        "video_link": videoLink,
        "slug": slug,
        "status": status,
        "goods_status": goodsStatus,
        "is_bulky": isBulky,
        "is_installment": isInstallment,
        "barcode": barcode,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "product_category_name": productCategoryName,
        "brand_name": brandName,
        "brand_info": brandInfo,
        "sale_off": saleOff,
        "gifts": List<dynamic>.from(gifts.map((x) => x)),
        "star": star,
        "colors": List<dynamic>.from(colors.map((x) => x.toJson())),
      };
}

class CategoryOfProductTogether {
  CategoryOfProductTogether({
    this.id,
    this.name,
    this.parent,
  });

  final int id;
  final String name;
  final dynamic parent;

  factory CategoryOfProductTogether.fromRawJson(String str) =>
      CategoryOfProductTogether.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryOfProductTogether.fromJson(Map<String, dynamic> json) => CategoryOfProductTogether(
        id: json["id"],
        name: json["name"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "parent": parent,
      };
}

class ColorOfProductTogether {
  ColorOfProductTogether({
    this.id,
    this.name,
    this.imageSource,
  });

  final int id;
  final String name;
  final List<String> imageSource;

  factory ColorOfProductTogether.fromRawJson(String str) => ColorOfProductTogether.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ColorOfProductTogether.fromJson(Map<String, dynamic> json) => ColorOfProductTogether(
        id: json["id"],
        name: json["name"],
        imageSource: List<String>.from(json["image_source"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": List<dynamic>.from(imageSource.map((x) => x)),
      };
}
