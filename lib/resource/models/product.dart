import 'dart:convert';

class Product {
  Product({
    this.status,
    this.data,
  });

  final int status;
  final List<DatumProduct> data;

  factory Product.fromRawJson(String str) => Product.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        status: json["status"],
        data: List<DatumProduct>.from(json["data"].map((x) => DatumProduct.fromJson(x))),
      );

  factory Product.fromJsonProduct(Map<String, dynamic> json ){
    return Product(
        status:  json['status'],
        data: List<DatumProduct>.from(
          json['data']['products'].map((x) => DatumProduct.fromJson(x)),
        )
    );
  }

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumProduct{
  DatumProduct({
    this.id,
    this.name,
    this.price,
    this.salePrice,
    this.productCategoryId,
    this.category,
    this.brandId,
    this.giftIds,
    this.imageSource,
    this.imageSourceList,
    this.description,
    this.content,
    this.specifications,
    this.videoLink,
    this.slug,
    this.status,
    this.goodsStatus,
    this.isBulky,
    this.isInstallment,
    this.barcode,
    this.createdAt,
    this.updatedAt,
    this.productCategoryName,
    this.brandName,
    this.brandInfo,
    this.saleOff,
    this.gifts,
    this.star,
    this.colors,
  });

  final int id;
  final String name;
  final int price;
  final int salePrice;
  final int productCategoryId;
  final CategoryOfProduct category;
  final int brandId;
  final List<int> giftIds;
  final String imageSource;
  final List<String> imageSourceList;
  final String description;
  final String content;
  final String specifications;
  final dynamic videoLink;
  final String slug;
  final int status;
  final int goodsStatus;
  final int isBulky;
  final int isInstallment;
  final String barcode;
  final String createdAt;
  final String updatedAt;
  final String productCategoryName;
  final String brandName;
  final dynamic brandInfo;
  final int saleOff;
  final List<Gift> gifts;
  final double star;
  final List<dynamic> colors;

  factory DatumProduct.fromRawJson(String str) => DatumProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DatumProduct.fromJson(Map<String, dynamic> json) => DatumProduct(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        salePrice: json["sale_price"],
        productCategoryId: json["product_category_id"],
        category: CategoryOfProduct.fromJson(json["category"]),
        brandId: json["brand_id"],
        giftIds: List<int>.from(json["gift_ids"].map((x) => x)),
        imageSource: json["image_source"],
        imageSourceList:
            List<String>.from(json["image_source_list"].map((x) => x)),
        description: json["description"],
        content: json["content"],
        specifications: json["specifications"],
        videoLink: json["video_link"],
        slug: json["slug"],
        status: json["status"],
        goodsStatus: json["goods_status"],
        isBulky: json["is_bulky"],
        isInstallment: json["is_installment"],
        barcode: json["barcode"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        productCategoryName: json["product_category_name"],
        brandName: json["brand_name"],
        brandInfo: json["brand_info"],
        saleOff: json["sale_off"],
        gifts: List<Gift>.from(json["gifts"].map((x) => Gift.fromJson(x))),
        star: json["star"].toDouble(),
        colors: List<dynamic>.from(json["colors"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "sale_price": salePrice,
        "product_category_id": productCategoryId,
        "category": category.toJson(),
        "brand_id": brandId,
        "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
        "image_source": imageSource,
        "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
        "description": description,
        "content": content,
        "specifications": specifications,
        "video_link": videoLink,
        "slug": slug,
        "status": status,
        "goods_status": goodsStatus,
        "is_bulky": isBulky,
        "is_installment": isInstallment,
        "barcode": barcode,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "product_category_name": productCategoryName,
        "brand_name": brandName,
        "brand_info": brandInfo,
        "sale_off": saleOff,
        "gifts": List<dynamic>.from(gifts.map((x) => x.toJson())),
        "star": star,
        "colors": List<dynamic>.from(colors.map((x) => x)),
      };
}

class CategoryOfProduct {
  CategoryOfProduct({
    this.id,
    this.name,
    this.parent,
  });

  final int id;
  final String name;
  final CategoryOfProduct parent;

  factory CategoryOfProduct.fromRawJson(String str) =>
      CategoryOfProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryOfProduct.fromJson(Map<String, dynamic> json) =>
      CategoryOfProduct(
        id: json["id"],
        name: json["name"],
        parent: json["parent"] == null
            ? null
            : CategoryOfProduct.fromJson(json["parent"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "parent": parent == null ? null : parent.toJson(),
      };
}

class Gift {
  Gift({
    this.id,
    this.name,
    this.price,
    this.imageSource,
    this.description,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final int price;
  final String imageSource;
  final dynamic description;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory Gift.fromRawJson(String str) => Gift.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Gift.fromJson(Map<String, dynamic> json) => Gift(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        imageSource: json["image_source"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "image_source": imageSource,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
