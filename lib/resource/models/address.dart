import 'dart:convert';

class Address {
  Address({
    this.status,
    this.data,
  });

  final int status;
  final List<DatumAddress> data;

  factory Address.fromRawJson(String str) => Address.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        status: json["status"],
        data: List<DatumAddress>.from(
            json["data"].map((x) => DatumAddress.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumAddress {
  DatumAddress({
    this.id,
    this.address,
    this.hotLine,
    this.openingHours,
    this.imageSource,
    this.latitude,
    this.longitude,
  });

  final int id;
  final String address;
  final String hotLine;
  final String openingHours;
  final String imageSource;
  final String latitude;
  final String longitude;

  factory DatumAddress.fromRawJson(String str) =>
      DatumAddress.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DatumAddress.fromJson(Map<String, dynamic> json) => DatumAddress(
        id: json["id"],
        address: json["address"],
        hotLine: json["hot_line"],
        openingHours: json["opening_hours"],
        imageSource: json["image_source"],
        latitude: json["latitude"],
        longitude: json["longitude"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address": address,
        "hot_line": hotLine,
        "opening_hours": openingHours,
        "image_source": imageSource,
        "latitude": latitude,
        "longitude": longitude,
      };
}
