import 'dart:convert';

class ProductQuestion {
  ProductQuestion({
    this.status,
    this.data,
  });

  final int status;
  final DataProductQuestion data;

  factory ProductQuestion.fromRawJson(String str) =>
      ProductQuestion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductQuestion.fromJson(Map<String, dynamic> json) =>
      ProductQuestion(
        status: json["status"],
        data: DataProductQuestion.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class DataProductQuestion {
  DataProductQuestion({
    this.rows,
    this.totalRecords,
  });

  final List<dynamic> rows;
  final int totalRecords;

  factory DataProductQuestion.fromRawJson(String str) =>
      DataProductQuestion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataProductQuestion.fromJson(Map<String, dynamic> json) =>
      DataProductQuestion(
        rows: List<dynamic>.from(json["rows"].map((x) => x)),
        totalRecords: json["total_records"],
      );

  Map<String, dynamic> toJson() => {
        "rows": List<dynamic>.from(rows.map((x) => x)),
        "total_records": totalRecords,
      };
}
