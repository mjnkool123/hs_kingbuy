import 'dart:convert';

class RelatedProduct {
  RelatedProduct({
    this.status,
    this.message,
    this.data,
  });

  final int status;
  final String message;
  final List<DatumRelatedProduct> data;

  factory RelatedProduct.fromRawJson(String str) =>
      RelatedProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RelatedProduct.fromJson(Map<String, dynamic> json) => RelatedProduct(
        status: json["status"],
        message: json["message"],
        data: List<DatumRelatedProduct>.from(json["data"].map((x) => DatumRelatedProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumRelatedProduct {
  DatumRelatedProduct({
    this.id,
    this.name,
    this.price,
    this.salePrice,
    this.productCategoryId,
    this.category,
    this.brandId,
    this.giftIds,
    this.imageSource,
    this.imageSourceList,
    this.description,
    this.content,
    this.specifications,
    this.videoLink,
    this.slug,
    this.status,
    this.goodsStatus,
    this.isBulky,
    this.isInstallment,
    this.barcode,
    this.createdAt,
    this.updatedAt,
    this.productCategoryName,
    this.brandName,
    this.brandInfo,
    this.saleOff,
    this.gifts,
    this.star,
    this.colors,
  });

  final int id;
  final String name;
  final int price;
  final int salePrice;
  final int productCategoryId;
  final CategoryOfRelatedProduct category;
  final int brandId;
  final List<int> giftIds;
  final String imageSource;
  final List<String> imageSourceList;
  final String description;
  final String content;
  final String specifications;
  final String videoLink;
  final String slug;
  final int status;
  final int goodsStatus;
  final int isBulky;
  final int isInstallment;
  final String barcode;
  final String createdAt;
  final String updatedAt;
  final String productCategoryName;
  final String brandName;
  final String brandInfo;
  final int saleOff;
  final List<GiftOfRelatedProduct> gifts;
  final double star;
  final List<ColorRelatedProduct> colors;

  factory DatumRelatedProduct.fromRawJson(String str) => DatumRelatedProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DatumRelatedProduct.fromJson(Map<String, dynamic> json) => DatumRelatedProduct(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        salePrice: json["sale_price"],
        productCategoryId: json["product_category_id"],
        category: CategoryOfRelatedProduct.fromJson(json["category"]),
        brandId: json["brand_id"],
        giftIds: List<int>.from(json["gift_ids"].map((x) => x)),
        imageSource: json["image_source"],
        imageSourceList:
            List<String>.from(json["image_source_list"].map((x) => x)),
        description: json["description"],
        content: json["content"],
        specifications: json["specifications"],
        videoLink: json["video_link"],
        slug: json["slug"],
        status: json["status"],
        goodsStatus: json["goods_status"],
        isBulky: json["is_bulky"],
        isInstallment: json["is_installment"],
        barcode: json["barcode"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        productCategoryName: json["product_category_name"],
        brandName: json["brand_name"],
        brandInfo: json["brand_info"],
        saleOff: json["sale_off"],
        gifts: List<GiftOfRelatedProduct>.from(json["gifts"].map((x) => GiftOfRelatedProduct.fromJson(x))),
        star: json["star"].toDouble(),
        colors: List<ColorRelatedProduct>.from(json["colors"].map((x) => ColorRelatedProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "sale_price": salePrice,
        "product_category_id": productCategoryId,
        "category": category.toJson(),
        "brand_id": brandId,
        "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
        "image_source": imageSource,
        "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
        "description": description,
        "content": content,
        "specifications": specifications,
        "video_link": videoLink,
        "slug": slug,
        "status": status,
        "goods_status": goodsStatus,
        "is_bulky": isBulky,
        "is_installment": isInstallment,
        "barcode": barcode,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "product_category_name": productCategoryName,
        "brand_name": brandName,
        "brand_info": brandInfo,
        "sale_off": saleOff,
        "gifts": List<dynamic>.from(gifts.map((x) => x.toJson())),
        "star": star,
        "colors": List<dynamic>.from(colors.map((x) => x.toJson())),
      };
}

class CategoryOfRelatedProduct {
  CategoryOfRelatedProduct({
    this.id,
    this.name,
    this.parent,
  });

  final int id;
  final String name;
  final CategoryOfRelatedProduct parent;

  factory CategoryOfRelatedProduct.fromRawJson(String str) =>
      CategoryOfRelatedProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryOfRelatedProduct.fromJson(Map<String, dynamic> json) => CategoryOfRelatedProduct(
        id: json["id"],
        name: json["name"],
        parent:
            json["parent"] == null ? null : CategoryOfRelatedProduct.fromJson(json["parent"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "parent": parent == null ? null : parent.toJson(),
      };
}

class ColorRelatedProduct {
  ColorRelatedProduct({
    this.id,
    this.name,
    this.imageSource,
  });

  final int id;
  final String name;
  final List<String> imageSource;

  factory ColorRelatedProduct.fromRawJson(String str) => ColorRelatedProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ColorRelatedProduct.fromJson(Map<String, dynamic> json) => ColorRelatedProduct(
        id: json["id"],
        name: json["name"],
        imageSource: List<String>.from(json["image_source"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": List<dynamic>.from(imageSource.map((x) => x)),
      };
}

class GiftOfRelatedProduct {
  GiftOfRelatedProduct({
    this.id,
    this.name,
    this.price,
    this.imageSource,
    this.description,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String name;
  final int price;
  final String imageSource;
  final dynamic description;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory GiftOfRelatedProduct.fromRawJson(String str) => GiftOfRelatedProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GiftOfRelatedProduct.fromJson(Map<String, dynamic> json) => GiftOfRelatedProduct(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        imageSource: json["image_source"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "image_source": imageSource,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
