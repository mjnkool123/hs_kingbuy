import 'dart:convert';

class ProductRatingInfo {
  ProductRatingInfo({
    this.status,
    this.data,
    this.message,
  });

  final int status;
  final DataProductRatingInfo data;
  final String message;

  factory ProductRatingInfo.fromRawJson(String str) =>
      ProductRatingInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductRatingInfo.fromJson(Map<String, dynamic> json) =>
      ProductRatingInfo(
        status: json["status"],
        data: DataProductRatingInfo.fromJson(json["data"]),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
        "message": message,
      };
}

class DataProductRatingInfo {
  DataProductRatingInfo({
    this.oneStarCount,
    this.twoStarCount,
    this.threeStarCount,
    this.fourStarCount,
    this.fiveStarCount,
    this.avgRating,
    this.ratingCount,
  });

  final int oneStarCount;
  final int twoStarCount;
  final int threeStarCount;
  final int fourStarCount;
  final int fiveStarCount;
  final int avgRating;
  final int ratingCount;

  factory DataProductRatingInfo.fromRawJson(String str) =>
      DataProductRatingInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataProductRatingInfo.fromJson(Map<String, dynamic> json) =>
      DataProductRatingInfo(
        oneStarCount: json["one_star_count"],
        twoStarCount: json["two_star_count"],
        threeStarCount: json["three_star_count"],
        fourStarCount: json["four_star_count"],
        fiveStarCount: json["five_star_count"],
        avgRating: json["avg_rating"],
        ratingCount: json["rating_count"],
      );

  Map<String, dynamic> toJson() => {
        "one_star_count": oneStarCount,
        "two_star_count": twoStarCount,
        "three_star_count": threeStarCount,
        "four_star_count": fourStarCount,
        "five_star_count": fiveStarCount,
        "avg_rating": avgRating,
        "rating_count": ratingCount,
      };
}
