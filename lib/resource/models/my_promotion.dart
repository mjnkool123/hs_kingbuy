import 'dart:convert';

class MyPromotion {
  MyPromotion({
    this.status,
    this.data,
  });

  final int status;
  final DataMyPromotion data;

  factory MyPromotion.fromRawJson(String str) => MyPromotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MyPromotion.fromJson(Map<String, dynamic> json) => MyPromotion(
    status: json["status"],
    data: DataMyPromotion.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class DataMyPromotion {
  DataMyPromotion({
    this.promotions,
    this.recordsTotal,
  });

  final List<MyPromotionElement> promotions;
  final int recordsTotal;

  factory DataMyPromotion.fromRawJson(String str) => DataMyPromotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataMyPromotion.fromJson(Map<String, dynamic> json) => DataMyPromotion(
    promotions: List<MyPromotionElement>.from(json["promotions"].map((x) => MyPromotionElement.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "promotions": List<dynamic>.from(promotions.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };
}

class MyPromotionElement {
  MyPromotionElement({
    this.id,
    this.title,
    this.imageSource,
    this.description,
    this.content,
    this.slug,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String title;
  final String imageSource;
  final String description;
  final String content;
  final String slug;
  final int status;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory MyPromotionElement.fromRawJson(String str) => MyPromotionElement.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MyPromotionElement.fromJson(Map<String, dynamic> json) => MyPromotionElement(
    id: json["id"],
    title: json["title"],
    imageSource: json["image_source"],
    description: json["description"],
    content: json["content"],
    slug: json["slug"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "image_source": imageSource,
    "description": description,
    "content": content,
    "slug": slug,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
