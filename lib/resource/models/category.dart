import 'dart:convert';

class Category {
  Category({
    this.status,
    this.data,
  });

  final int status;
  final DataCategory data;

  factory Category.fromRawJson(String str) =>
      Category.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        status: json["status"],
        data: DataCategory.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class DataCategory {
  DataCategory({
    this.categories,
    this.recordsTotal,
  });

  final List<CategoryElement> categories;
  final int recordsTotal;

  factory DataCategory.fromRawJson(String str) => DataCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataCategory.fromJson(Map<String, dynamic> json) => DataCategory(
        categories: List<CategoryElement>.from(
            json["categories"].map((x) => CategoryElement.fromJson(x))),
        recordsTotal: json["recordsTotal"],
      );

  Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "recordsTotal": recordsTotal,
      };
}

class CategoryElement {
  CategoryElement({
    this.id,
    this.name,
    this.imageSource,
    this.iconSource,
    this.backgroundImage,
    this.parentId,
    this.parent,
    this.children,
    this.description,
    this.slug,
    this.videoLink,
  });

  final int id;
  final String name;
  final String imageSource;
  final String iconSource;
  final String backgroundImage;
  final int parentId;
  final Parent parent;
  final List<CategoryElement> children;
  final String description;
  final String slug;
  final String videoLink;

  factory CategoryElement.fromRawJson(String str) =>
      CategoryElement.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategoryElement.fromJson(Map<String, dynamic> json) =>
      CategoryElement(
        id: json["id"],
        name: json["name"],
        imageSource: json["image_source"],
        iconSource: json["icon_source"] == null ? null : json["icon_source"],
        backgroundImage:
            json["background_image"] == null ? null : json["background_image"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        parent: json["parent"] == null ? null : Parent.fromJson(json["parent"]),
        children: List<CategoryElement>.from(
            json["children"].map((x) => CategoryElement.fromJson(x))),
        description: json["description"] == null ? null : json["description"],
        slug: json["slug"],
        videoLink: json["video_link"] == null ? null : json["video_link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": imageSource,
        "icon_source": iconSource == null ? null : iconSource,
        "background_image": backgroundImage == null ? null : backgroundImage,
        "parent_id": parentId == null ? null : parentId,
        "parent": parent == null ? null : parent.toJson(),
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
        "description": description == null ? null : description,
        "slug": slug,
        "video_link": videoLink == null ? null : videoLink,
      };
}

class Parent {
  Parent({
    this.id,
    this.name,
    this.slug,
    this.videoLink,
    this.parent,
  });

  final int id;
  final String name;
  final String slug;
  final String videoLink;
  final dynamic parent;

  factory Parent.fromRawJson(String str) => Parent.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Parent.fromJson(Map<String, dynamic> json) => Parent(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        videoLink: json["video_link"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "video_link": videoLink,
        "parent": parent,
      };
}
