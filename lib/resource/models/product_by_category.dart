// import 'dart:convert';
//
// class ProductByCategory {
//   ProductByCategory({
//     this.status,
//     this.data,
//   });
//
//   final int status;
//   final DataProductByCategory data;
//
//   factory ProductByCategory.fromRawJson(String str) =>
//       ProductByCategory.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory ProductByCategory.fromJson(Map<String, dynamic> json) =>
//       ProductByCategory(
//         status: json["status"],
//         data: DataProductByCategory.fromJson(json["data"]),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "status": status,
//         "data": data.toJson(),
//       };
// }
//
// class DataProductByCategory {
//   DataProductByCategory({
//     this.id,
//     this.name,
//     this.products,
//     this.recordsTotal,
//   });
//
//   final int id;
//   final Name name;
//   final List<ProductByCate> products;
//   final int recordsTotal;
//
//   factory DataProductByCategory.fromRawJson(String str) =>
//       DataProductByCategory.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory DataProductByCategory.fromJson(Map<String, dynamic> json) =>
//       DataProductByCategory(
//         id: json["id"],
//         name: nameValues.map[json["name"]],
//         products: List<ProductByCate>.from(
//             json["products"].map((x) => ProductByCate.fromJson(x))),
//         recordsTotal: json["recordsTotal"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": nameValues.reverse[name],
//         "products": List<dynamic>.from(products.map((x) => x.toJson())),
//         "recordsTotal": recordsTotal,
//       };
// }
//
// enum Name { GH_MASSAGE_THNG_GIA, GH_MASSAGE }
//
// final nameValues = EnumValues({
//   "Ghế massage": Name.GH_MASSAGE,
//   "Ghế massage thương gia": Name.GH_MASSAGE_THNG_GIA
// });
//
// class ProductByCate {
//   ProductByCate({
//     this.id,
//     this.name,
//     this.price,
//     this.salePrice,
//     this.productCategoryId,
//     this.category,
//     this.brandId,
//     this.giftIds,
//     this.imageSource,
//     this.imageSourceList,
//     this.description,
//     this.content,
//     this.specifications,
//     this.videoLink,
//     this.slug,
//     this.status,
//     this.goodsStatus,
//     this.isBulky,
//     this.isInstallment,
//     this.barcode,
//     this.createdAt,
//     this.updatedAt,
//     this.productCategoryName,
//     this.brandName,
//     this.brandInfo,
//     this.saleOff,
//     this.gifts,
//     this.star,
//     this.colors,
//   });
//
//   final int id;
//   final String name;
//   final int price;
//   final int salePrice;
//   final int productCategoryId;
//   final CategoryOfProductByCategory category;
//   final int brandId;
//   final List<int> giftIds;
//   final String imageSource;
//   final List<String> imageSourceList;
//   final String description;
//   final String content;
//   final String specifications;
//   final dynamic videoLink;
//   final String slug;
//   final int status;
//   final int goodsStatus;
//   final int isBulky;
//   final int isInstallment;
//   final String barcode;
//   final String createdAt;
//   final String updatedAt;
//   final Name productCategoryName;
//   final String brandName;
//   final dynamic brandInfo;
//   final int saleOff;
//   final List<GiftOfProductByCategory> gifts;
//   final double star;
//   final List<Color> colors;
//
//   factory ProductByCate.fromRawJson(String str) =>
//       ProductByCate.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory ProductByCate.fromJson(Map<String, dynamic> json) => ProductByCate(
//         id: json["id"],
//         name: json["name"],
//         price: json["price"],
//         salePrice: json["sale_price"],
//         productCategoryId: json["product_category_id"],
//         category: CategoryOfProductByCategory.fromJson(json["category"]),
//         brandId: json["brand_id"],
//         giftIds: List<int>.from(json["gift_ids"].map((x) => x)),
//         imageSource: json["image_source"],
//         imageSourceList:
//             List<String>.from(json["image_source_list"].map((x) => x)),
//         description: json["description"],
//         content: json["content"],
//         specifications:
//             json["specifications"] == null ? null : json["specifications"],
//         videoLink: json["video_link"],
//         slug: json["slug"],
//         status: json["status"],
//         goodsStatus: json["goods_status"],
//         isBulky: json["is_bulky"],
//         isInstallment: json["is_installment"],
//         barcode: json["barcode"],
//         createdAt: json["created_at"],
//         updatedAt: json["updated_at"],
//         productCategoryName: nameValues.map[json["product_category_name"]],
//         brandName: json["brand_name"],
//         brandInfo: json["brand_info"],
//         saleOff: json["sale_off"],
//         gifts: List<GiftOfProductByCategory>.from(
//             json["gifts"].map((x) => GiftOfProductByCategory.fromJson(x))),
//         star: json["star"].toDouble(),
//         colors: List<Color>.from(json["colors"].map((x) => Color.fromJson(x))),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "price": price,
//         "sale_price": salePrice,
//         "product_category_id": productCategoryId,
//         "category": category.toJson(),
//         "brand_id": brandId,
//         "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
//         "image_source": imageSource,
//         "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
//         "description": description,
//         "content": content,
//         "specifications": specifications == null ? null : specifications,
//         "video_link": videoLink,
//         "slug": slug,
//         "status": status,
//         "goods_status": goodsStatus,
//         "is_bulky": isBulky,
//         "is_installment": isInstallment,
//         "barcode": barcode,
//         "created_at": createdAt,
//         "updated_at": updatedAt,
//         "product_category_name": nameValues.reverse[productCategoryName],
//         "brand_name": brandName,
//         "brand_info": brandInfo,
//         "sale_off": saleOff,
//         "gifts": List<dynamic>.from(gifts.map((x) => x.toJson())),
//         "star": star,
//         "colors": List<dynamic>.from(colors.map((x) => x.toJson())),
//       };
// }
//
// enum BrandName { OTO }
//
// final brandNameValues = EnumValues({"OTO": BrandName.OTO});
//
// class CategoryOfProductByCategory {
//   CategoryOfProductByCategory({
//     this.id,
//     this.name,
//     this.parent,
//   });
//
//   final int id;
//   final Name name;
//   final CategoryOfProductByCategory parent;
//
//   factory CategoryOfProductByCategory.fromRawJson(String str) =>
//       CategoryOfProductByCategory.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory CategoryOfProductByCategory.fromJson(Map<String, dynamic> json) =>
//       CategoryOfProductByCategory(
//         id: json["id"],
//         name: nameValues.map[json["name"]],
//         parent: json["parent"] == null
//             ? null
//             : CategoryOfProductByCategory.fromJson(json["parent"]),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": nameValues.reverse[name],
//         "parent": parent == null ? null : parent.toJson(),
//       };
// }
//
// class Color {
//   Color({
//     this.id,
//     this.name,
//     this.imageSource,
//   });
//
//   final int id;
//   final String name;
//   final List<String> imageSource;
//
//   factory Color.fromRawJson(String str) => Color.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory Color.fromJson(Map<String, dynamic> json) => Color(
//         id: json["id"],
//         name: json["name"],
//         imageSource: List<String>.from(json["image_source"].map((x) => x)),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "image_source": List<dynamic>.from(imageSource.map((x) => x)),
//       };
// }
//
// class GiftOfProductByCategory {
//   GiftOfProductByCategory({
//     this.id,
//     this.name,
//     this.price,
//     this.imageSource,
//     this.description,
//     this.createdAt,
//     this.updatedAt,
//   });
//
//   final int id;
//   final String name;
//   final int price;
//   final String imageSource;
//   final dynamic description;
//   final DateTime createdAt;
//   final DateTime updatedAt;
//
//   factory GiftOfProductByCategory.fromRawJson(String str) =>
//       GiftOfProductByCategory.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory GiftOfProductByCategory.fromJson(Map<String, dynamic> json) =>
//       GiftOfProductByCategory(
//         id: json["id"],
//         name: json["name"],
//         price: json["price"],
//         imageSource: json["image_source"],
//         description: json["description"],
//         createdAt: DateTime.parse(json["created_at"]),
//         updatedAt: DateTime.parse(json["updated_at"]),
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "price": price,
//         "image_source": imageSource,
//         "description": description,
//         "created_at": createdAt.toIso8601String(),
//         "updated_at": updatedAt.toIso8601String(),
//       };
// }
//
// class EnumValues<T> {
//   Map<String, T> map;
//   Map<T, String> reverseMap;
//
//   EnumValues(this.map);
//
//   Map<T, String> get reverse {
//     if (reverseMap == null) {
//       reverseMap = map.map((k, v) => new MapEntry(v, k));
//     }
//     return reverseMap;
//   }
// }
