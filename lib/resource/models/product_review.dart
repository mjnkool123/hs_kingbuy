import 'dart:convert';

class ProductReview {
  ProductReview({
    this.status,
    this.data,
  });

  final int status;
  final DataProductReview data;

  factory ProductReview.fromRawJson(String str) =>
      ProductReview.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductReview.fromJson(Map<String, dynamic> json) => ProductReview(
        status: json["status"],
        data: DataProductReview.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
      };
}

class DataProductReview {
  DataProductReview({
    this.rows,
    this.totalRecords,
  });

  final List<RowReview> rows;
  final int totalRecords;

  factory DataProductReview.fromRawJson(String str) =>
      DataProductReview.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataProductReview.fromJson(Map<String, dynamic> json) =>
      DataProductReview(
        rows: List<RowReview>.from(
            json["rows"].map((x) => RowReview.fromJson(x))),
        totalRecords: json["total_records"],
      );

  Map<String, dynamic> toJson() => {
        "rows": List<dynamic>.from(rows.map((x) => x.toJson())),
        "total_records": totalRecords,
      };
}

class RowReview {
  RowReview({
    this.id,
    this.userId,
    this.productId,
    this.name,
    this.phoneNumber,
    this.comment,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.star,
    this.avatarSource,
    this.isBuy,
  });

  final int id;
  final int userId;
  final int productId;
  final String name;
  final String phoneNumber;
  final String comment;
  final int status;
  final DateTime createdAt;
  final DateTime updatedAt;
  final double star;
  final String avatarSource;
  final int isBuy;

  factory RowReview.fromRawJson(String str) =>
      RowReview.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RowReview.fromJson(Map<String, dynamic> json) => RowReview(
        id: json["id"],
        userId: json["user_id"],
        productId: json["product_id"],
        name: json["name"],
        phoneNumber: json["phone_number"],
        comment: json["comment"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        star: json["star"].toDouble(),
        avatarSource: json["avatar_source"],
        isBuy: json["is_buy"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "product_id": productId,
        "name": name,
        "phone_number": phoneNumber,
        "comment": comment,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "star": star,
        "avatar_source": avatarSource,
        "is_buy": isBuy,
      };
}
