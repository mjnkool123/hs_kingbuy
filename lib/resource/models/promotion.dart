import 'dart:convert';

class Promotion {
  Promotion({
    this.status,
    this.data,
  });

  final int status;
  final DataPromotion data;

  factory Promotion.fromRawJson(String str) => Promotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Promotion.fromJson(Map<String, dynamic> json) => Promotion(
    status: json["status"],
    data: DataPromotion.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };

  @override
  String toString() {
    return 'Promontion{status: $status, data: $data}';
  }
}

class DataPromotion {
  DataPromotion({
    this.news,
    this.recordsTotal,
  });

  final List<NewsPromotion> news;
  final int recordsTotal;

  factory DataPromotion.fromRawJson(String str) => DataPromotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DataPromotion.fromJson(Map<String, dynamic> json) => DataPromotion(
    news: List<NewsPromotion>.from(json["news"].map((x) => NewsPromotion.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "news": List<dynamic>.from(news.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };

  @override
  String toString() {
    return 'Data{news: $news, recordsTotal: $recordsTotal}';
  }
}

class NewsPromotion {
  NewsPromotion({
    this.id,
    this.title,
    this.imageSource,
    this.description,
    this.content,
    this.slug,
    this.isAutoSlug,
    this.seoTitle,
    this.seoDescription,
    this.checkPromotion,
    this.status,
    this.isFeatured,
    this.order,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String title;
  final String imageSource;
  final String description;
  final String content;
  final String slug;
  final int isAutoSlug;
  final String seoTitle;
  final String seoDescription;
  final int checkPromotion;
  final int status;
  final int isFeatured;
  final int order;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory NewsPromotion.fromRawJson(String str) => NewsPromotion.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory NewsPromotion.fromJson(Map<String, dynamic> json) => NewsPromotion(
    id: json["id"],
    title: json["title"],
    imageSource: json["image_source"],
    description: json["description"],
    content: json["content"],
    slug: json["slug"],
    isAutoSlug: json["is_auto_slug"],
    seoTitle: json["seo_title"],
    seoDescription: json["seo_description"] == null ? null : json["seo_description"],
    checkPromotion: json["check_promotion"],
    status: json["status"],
    isFeatured: json["is_featured"],
    order: json["order"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "image_source": imageSource,
    "description": description,
    "content": content,
    "slug": slug,
    "is_auto_slug": isAutoSlug,
    "seo_title": seoTitle,
    "seo_description": seoDescription == null ? null : seoDescription,
    "check_promotion": checkPromotion,
    "status": status,
    "is_featured": isFeatured,
    "order": order,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };

  @override
  String toString() {
    return 'News{id: $id, title: $title, imageSource: $imageSource, description: $description, content: $content, slug: $slug, isAutoSlug: $isAutoSlug, seoTitle: $seoTitle, seoDescription: $seoDescription, checkPromotion: $checkPromotion, status: $status, isFeatured: $isFeatured, order: $order, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}
