import 'package:dio/dio.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/resource/service/service.dart';
import 'package:http/http.dart' as http;

class CategoryRepository {
  http.Client client = http.Client();
  String _apiCategories = AppEndpoint.CATEGORIES;

  Future<Category> fetchCategory(int limit, offset) async {
    Map<String, dynamic> params = {"limit": limit, "offset": offset};
    try {
      final response = await DioService().get(_apiCategories, queryParameters: params);
      return Category.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }
}
