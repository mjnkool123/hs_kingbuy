import 'package:dio/dio.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/resource/service/service.dart';
import 'package:http/http.dart' as http;

class PromotionRepository {
  http.Client client = http.Client();
  String _apiPromotion = AppEndpoint.ALLPROMONTION;
  String _apiMyPromotion = AppEndpoint.ALLMYPROMOTION;

  Future<Promotion> fetchPromotion(int limit, offset) async {
    Map<String, dynamic> params = {"limit": limit, "offset": offset};
    try {
      final response = await DioService().get(_apiPromotion, queryParameters: params);
      return Promotion.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<MyPromotion> fetchMyPromotion(int limit, offset) async {
    Map<String, dynamic> params = {"limit": limit, "offset": offset};
    try {
      final response = await DioService().get(_apiMyPromotion,queryParameters: params);
      return MyPromotion.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }
}
