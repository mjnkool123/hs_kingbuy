import 'package:dio/dio.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/resource/service/service.dart';
import 'package:http/http.dart' as http;

class SearchRepository {
  http.Client client = http.Client();
  String _apiSearch = AppEndpoint.SEARCH;

  Future<Product> fetchSearch(String searchWord) async {
    Map<String, dynamic> params = {"searchWord": searchWord};
    try {
      final response = await DioService().get(_apiSearch, queryParameters: params);
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<Product> fetchSearchByCategory(int productCategoryId) async {
    Map<String, dynamic> params = {"product_category_id": productCategoryId};
    try {
      final response = await DioService().get(_apiSearch, queryParameters: params);
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

}