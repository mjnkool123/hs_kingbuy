import 'package:dio/dio.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/resource/service/service.dart';
import 'package:http/http.dart' as http;

class ProductRepository {
  http.Client client = http.Client();
  String _apiProductByCategory = AppEndpoint.PRODUCTSBYCATEGORY;
  String _apiAllProductNew = AppEndpoint.ALLPRODUCTNEW;
  String _apiReviewProduct = AppEndpoint.REVIEWPRODUCT;
  String _apiRatingInfo = AppEndpoint.RATINGINFO;
  String _apiProductQuestion = AppEndpoint.PRODUCTQUESTION;
  String _apiPurchasedTogetherProduct = AppEndpoint.PURCHASEDTOGETHERPRODUCT;
  String _apiRelatedProduct = AppEndpoint.RELATEDPRODUCT;

  Future<Product> fetchAllProductNew(int limit, offset) async {
    Map<String, dynamic> params = {"limit": limit, "offset": offset};
    try {
      final response =
          await DioService().get(_apiAllProductNew, queryParameters: params);
      return Product.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<Product> fetchProductByCategory(int id) async {
    Map<String, dynamic> params = {"product_category_id": id};

    try {
      final response = await DioService()
          .get(_apiProductByCategory, queryParameters: params);
      return Product.fromJsonProduct(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<ProductReview> fetchReviewProduct(int id) async {
    Map<String, dynamic> params = {"product_id": id};
    try {
      final response =
      await DioService().get(_apiReviewProduct, queryParameters: params);
      return ProductReview.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<ProductRatingInfo> fetchRatingInfo(int id) async {
    Map<String, dynamic> params = {"product_id": id};
    try {
      final response =
      await DioService().get(_apiRatingInfo, queryParameters: params);
      return ProductRatingInfo.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<ProductQuestion> fetchProductQuestion(int id) async {
    Map<String, dynamic> params = {"product_id": id};

    try {
      final response = await DioService()
          .get(_apiProductQuestion, queryParameters: params);
      return ProductQuestion.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<PurchasedTogetherProduct> fetchPurchasedTogetherProduct(int id, int limit, int offset) async {
    Map<String, dynamic> params = {"product_id": id, "limit": limit, "offset": offset};

    try {
      final response = await DioService()
          .get(_apiPurchasedTogetherProduct, queryParameters: params);
      return PurchasedTogetherProduct.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

  Future<RelatedProduct> fetchRelatedProduct(int id, int limit, int offset) async {
    Map<String, dynamic> params = {"limit": limit, "offset": offset};

    try {
      final response = await DioService()
          .get(_apiRelatedProduct+ "/$id", queryParameters: params);
      return RelatedProduct.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }

}
