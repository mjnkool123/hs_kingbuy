import 'package:dio/dio.dart';
import 'package:hs_kingbuy/configs/configs.dart';
import 'package:hs_kingbuy/resource/resource.dart';
import 'package:hs_kingbuy/resource/service/service.dart';
import 'package:http/http.dart' as http;

class AddressRepository {
  http.Client client = http.Client();
  String _apiAddress = AppEndpoint.ALLADDRESS;

  Future<Address> fetchAddress() async {
    try {
      final response = await DioService().get(_apiAddress);
      return Address.fromJson(response.data);
    } on DioError catch (e) {
      print(e.error);
    }
  }
}