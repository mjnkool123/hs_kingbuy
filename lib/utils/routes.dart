import 'package:flutter/material.dart';
import 'package:hs_kingbuy/presentation/presentation.dart';

class Routes {
  //login
  static const String loginScreen = 'login_screen';
  static const String signupScreen = 'signup_screen';

  //home
  static const String searchScreen = 'search_screen';
  static const String searchResult = 'search_result';


  static final Map<String, WidgetBuilder> routes = {
    //login
    loginScreen: (_) => LoginScreen(),

    //home
    searchScreen: (_) => SearchScreen(),
    // searchResult: (_) => SearchResult(),
  };
}
